const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const typeorm = require("typeorm");


const routes = () => {

    typeorm.createConnection().then(connection => {
        app.use(express.static('public'));
        app.use(express.json());
        app.use(cookieParser())

        const ControllerPath = './../src/Controller';

        const index = require(ControllerPath+'/Index');
        app.use('/', index);

        const oAuth = require(ControllerPath+'/OAuth');
        app.use('/oAuth', oAuth);

        const user = require(ControllerPath+'/User');
        app.use('/user', user);

        const subEvent = require(ControllerPath+'/SubEvent');
        app.use('/subEvent', subEvent);

        const subEventNew = require(ControllerPath+'/SubEventNew');
        app.use('/subEventNew', subEventNew);

        const hypeTrain = require(ControllerPath+'/Subevents/HypeTrain');
        app.use('/hypeTrain', hypeTrain);

        const hypeTrainDbConnector = require(ControllerPath+'/Subevents/HypeTrainDbConnector');
        app.use('/hypeTrainDbConnector', hypeTrainDbConnector);

        const chat = require(ControllerPath+'/Chat');
        app.use('/', chat.index());

        const overlay = require(ControllerPath+'/Overlay');
        app.use('/', overlay.index());

        const connector = require(ControllerPath+'/Connector');
        app.use('/', connector.index());

        app.listen(process.env.PORT, () => {
            console.log(`App is listening at http://localhost:${process.env.PORT}`);
        });

    });
}

/**
 * name - route pair
 * Set name to use it in twig template as {{ path() }} extension
 * Set route path to output e.g. {{ path('wonderfulName') }} => /article/NewYear2022
 * route.set('wonderfulName', '/article/NewYear2022');
 * @returns object
 */
const paths = () => {
    let route = new Map();

    route.set('index', '/');
    route.set('twitch', 'twitch');
    route.set('OAuth', 'oAuth');
    route.set('user', 'user');
    route.set('subEvent', 'subEvent');
    route.set('subEventNew', 'subEventNew');
    route.set('hypeTrain', 'hypeTrain');
    route.set('hypeTrainDbConnector', 'hypeTrainDbConnector');
    route.set('chat', 'chat');
    route.set('chatToken', 'chat-token');
    route.set('chatCommand', 'chat-command');
    route.set('overlay', 'overlay');
    route.set('overlay-default', 'overlay-default');
    route.set('overlay-light', 'overlay-light');
    route.set('overlay-dark', 'overlay-dark');



    return route;
}

exports.paths = paths;
exports.routes = routes;