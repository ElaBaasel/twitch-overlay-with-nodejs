const localtunnel = require('localtunnel');
const orm = require("typeorm");
const Tunnel = require("./../src/Entity/Tunnel");
const Cryptr = require("cryptr");

const set = async  () => {

    await (async () => {
        const tunnel = await localtunnel({port: 3000});

        // the assigned public url for your tunnel
        // i.e. https://abcdefgjhij.localtunnel.me
        const cryptr = new Cryptr(process.env.TUNNELPASS);

        const url = cryptr.encrypt(tunnel.url);
        const getData = await orm.getRepository(Tunnel)
            .createQueryBuilder()
            .getOne();


        if (getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .update(Tunnel)
                .set({
                    url: url
                })
                .where("id = :id", { id: 1 })
                .execute();
        }

        if (!getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .insert()
                .into(Tunnel)
                .values([{
                    url: url
                }
                ])
                .execute();
        }

        tunnel.on('close', () => {
            // tunnels are closed
        });
    })();

}

exports.set = set;