const Twig = require('twig'),
    express = require('express'),
    app = express();
const routes = require('./routes');
const moment = require('moment');
const Cryptr = require("cryptr");

const config = () => {
    app.set("twig options", {
        allow_async: true, // Allow asynchronous compiling
        strict_variables: false
    });

    Twig.extendFunction("path", function(value) {

        const paths = routes.paths();
        for (let [key, item] of paths) {
            if (key === value) {
                return item;
            }
        }

    });

    Twig.extendFilter("formatDate", function(value) {
        return moment(value).format('DD.MM.YYYY - HH:mm:ss');
    });

    const cryptr = new Cryptr(process.env.TUNNELPASS);

    Twig.extendFilter("encrypt", function(value) {
        return cryptr.encrypt(value);
    });


}

exports.config = config;
