require('dotenv').config();
const routes = require('./config/routes');
const twigConfig = require('./config/twig');
const database = require('./config/database');
//const tunnel = require('./config/tunnel');


twigConfig.config();
routes.routes();
//tunnel.set();


