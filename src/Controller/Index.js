const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const router = express.Router();

router.get('/', function (req, res, next) {

    res.render(path.join(__dirname+'/../../templates/dashboard.html.twig'), {
        message : "Hello World"
    });

});


module.exports = router;