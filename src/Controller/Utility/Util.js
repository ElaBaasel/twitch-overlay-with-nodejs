const ret = {};
const iterate = (obj) => {
    Object.keys(obj).forEach(key => {
        if (typeof obj[key] !== 'object' && obj[key] !== null) {
            //console.log(`key: ${key}, value: ${obj[key]}`)
            ret[key] = obj[key];
        }
        if (typeof obj[key] === 'object') {
            iterate(obj[key])
        }
    })
    return ret;
};

exports.iterate = iterate;