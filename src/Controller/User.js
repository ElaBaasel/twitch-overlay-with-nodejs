const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const csrf = require("csurf");
const bodyParser = require("body-parser");
const router = express.Router();
const httpReq = require('./HttpRequest');
const util = require('./Utility/Util')
const orm = require("typeorm");
const TwitchConsole = require("./../Entity/TwitchConsole");
const TwitchConsoleRepo = require("./../Repository/TwitchConsoleRepo");
const twitchOAuth = require("./../Entity/TwitchOAuthToken");

const csrfProtection = csrf({cookie: true});
const parseForm = bodyParser.urlencoded({extended: false});

router.get('/', csrfProtection, async function (req, res, next) {

    res.render(path.join(__dirname+'/../../templates/user.html.twig'), {
        csrfToken: req.csrfToken()
    });

});

router.post('/', parseForm, csrfProtection, async function (req, res) {


    let buttonName = req.body.buttonNameSave;

    let userName = req.body.userName;
    let userId = req.body.userId
    let saveUserName = req.body.saveUserName

    if (typeof buttonName !== 'undefined' && buttonName === 'UserName[SubmitButton]') {

        if (!userName) {
            return Twig.renderFile(path.join(__dirname+'/../../templates/AlertMsg/InputField.html.twig'), {
                msg : "Empty input field",
                additionalMsg : "Please fill input field",
                class: "empty-input-userName"
            }, (err, html) => {
                res.json({
                    msg: 'error',
                    html: html
                })
            });
        }

        if (userName) {
            const oAuth = await orm.getRepository(twitchOAuth)
                .createQueryBuilder()
                .getOne();

            const twitchConsole = await TwitchConsoleRepo.getData();

            const rawData = await httpReq.getUserId(oAuth.token, twitchConsole.client_id, userName);

            const userId = util.iterate(rawData.data).id

            return Twig.renderFile(path.join(__dirname+'/../../templates/InfoMsg/InfoPopup.html.twig'), {
                class: 'showUserId',
                msg : userId
            }, (err, html) => {
                res.json({
                    msg: 'info',
                    html: html
                })
            });
        }
    }

    if (typeof buttonName !== 'undefined' && buttonName === 'UserId[SubmitButton]') {

        if (!userId) {
            return Twig.renderFile(path.join(__dirname+'/../../templates/AlertMsg/InputField.html.twig'), {
                msg : "Empty input field",
                additionalMsg : "Please fill input field",
                class: "empty-input-userId"
            }, (err, html) => {
                res.json({
                    msg: 'error',
                    html: html
                })
            });
        }

        if (userId) {

            const getData = await TwitchConsoleRepo.getData();

            if (getData) {
                await orm.getConnection()
                    .createQueryBuilder()
                    .update(TwitchConsole)
                    .set({
                        user_id: userId
                    })
                    .where("id = :id", {id: 1})
                    .execute();
            }

            if (!getData) {
                await orm.getConnection()
                    .createQueryBuilder()
                    .insert()
                    .into(TwitchConsole)
                    .values([{
                        user_id: userId
                    }
                    ])
                    .execute();
            }

            return Twig.renderFile(path.join(__dirname+'/../../templates/InfoMsg/InfoPopup.html.twig'), {
                class: 'storeUserId',
                msg : 'User Id stored in the database'
            }, (err, html) => {
                res.json({
                    msg: 'info',
                    html: html
                })
            });
        }
    }

    if (typeof saveUserName !== 'undefined' && buttonName === 'user[submitSaveUsername]') {

        if (!saveUserName) {
            return Twig.renderFile(path.join(__dirname+'/../../templates/AlertMsg/InputField.html.twig'), {
                msg : "Empty input field",
                additionalMsg : "Please fill input field",
                class: "empty-input-userId"
            }, (err, html) => {
                res.json({
                    msg: 'error',
                    html: html
                })
            });
        }

        if (saveUserName) {

            const getData = await TwitchConsoleRepo.getData()

            if (getData) {
                await orm.getConnection()
                    .createQueryBuilder()
                    .update(TwitchConsole)
                    .set({
                        username: saveUserName
                    })
                    .where("id = :id", {id: 1})
                    .execute();
            }

            if (!getData) {
                await orm.getConnection()
                    .createQueryBuilder()
                    .insert()
                    .into(TwitchConsole)
                    .values([{
                        username: saveUserName
                    }
                    ])
                    .execute();
            }

            return Twig.renderFile(path.join(__dirname+'/../../templates/InfoMsg/InfoPopup.html.twig'), {
                class: 'storeUserId',
                msg : 'Username stored in the database'
            }, (err, html) => {
                res.json({
                    msg: 'info',
                    html: html
                })
            });
        }
    }

});



module.exports = router;