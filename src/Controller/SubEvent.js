const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const router = express.Router();
const httpReq = require('./HttpRequest');
const Cryptr = require("cryptr");
const orm = require("typeorm");
const twitchOAuth = require("./../Entity/TwitchOAuthToken");
const TwitchConsoleRepo = require("../Repository/TwitchConsoleRepo");
const cryptr = new Cryptr(process.env.TUNNELPASS);

router.get('/', async function (req, res, next) {

    const oAuth = await orm.getRepository(twitchOAuth)
        .createQueryBuilder()
        .getOne();

    const twitchConsole = await TwitchConsoleRepo.getData();

    const clientId = twitchConsole.client_id;
    const token = oAuth.token;

    const subEventListData = await httpReq.getSubEventList(clientId, token);

    res.render(path.join(__dirname + '/../../templates/subEvent.html.twig'), {
        subEvent: subEventListData.data,
    });

});

router.post('/', async function (req, res) {

    const eventId = req.body.eventId
    const secret = req.body.secret

    if (typeof eventId !== 'undefined') {
        const oAuth = await orm.getRepository(twitchOAuth)
            .createQueryBuilder()
            .getOne();

        const twitchConsole = await TwitchConsoleRepo.getData();

        const data = await httpReq.deleteEventSubscriptions(oAuth.token, twitchConsole.client_id, eventId);
        res.json({
            msg: 'reloadPage'
        })
    }


    if (typeof secret !== 'undefined') {
        const url = cryptr.decrypt(secret)
        res.json({
            msg: 'encodedUrl',
            url: url
        })
    }

});

module.exports = router;