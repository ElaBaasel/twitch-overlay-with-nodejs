const si = require('systeminformation');
const WebSocketServer = require('websocket').server;
const WebSocketClient = require('websocket').client;
const http = require('http');
const osu = require('node-os-utils')
const cpu = osu.cpu
const netstat = osu.netstat

const systeminformation = async function (req, res, next) {
    const request = req.method;

    if (request === 'POST') await handlePostRequest(req, res);



    if (request === 'GET') {

/*        setInterval(function() {
            cpu.usage()
                .then(cpuPercentage => {
                    console.log(cpuPercentage) // 10.38
                })
        }, 500)*/

        let wsServer;
        const interval = req.query.interval

        const server = http.createServer(function (request, response) {
            console.log((new Date()) + ' Received request for ' + request.url);
            response.writeHead(404);
            response.end();
        });
        server.listen(6090);

        wsServer = new WebSocketServer({
            httpServer: server,
            // You should not use autoAcceptConnections for production
            // applications, as it defeats all standard cross-origin protection
            // facilities built into the protocol and the browser.  You should
            // *always* verify the connection's origin and decide whether or not
            // to accept it.
            autoAcceptConnections: false
        });

        wsServer.on('client', function () {
            console.log(request);
        })
        function originIsAllowed(origin) {
            // put logic here to detect whether the specified origin is allowed.
            return true;
        }

        wsServer.on('request', function (request) {
            if (!originIsAllowed(request.origin)) {
                // Make sure we only accept requests from an allowed origin
                request.reject();
                return;
            }

            const connection = request.accept('echo-protocol', request.origin);

            connection.on('message', function (message) {
                if (message.type === 'utf8') {

                    setInterval(function() {
                        let systemInfos = {};





                        si.networkStats('Ethernet').then(data => {

                            let incomingInMByte =  (data[0].rx_sec / (1024*1024));
                            let incomingInKByte =  (data[0].rx_sec / 1024);

                            let outgoingInMByte =  (data[0].tx_sec / (1024*1024));
                            let outgoingInKByte =  (data[0].tx_sec / 1024);

                            systemInfos['incomingMb'] = incomingInMByte.toFixed(2);
                            systemInfos['incomingKb'] = incomingInKByte.toFixed(2);
                            systemInfos['outgoingMb'] = outgoingInMByte.toFixed(2);
                            systemInfos['outgoingKb'] = outgoingInKByte.toFixed(2);

                            let newPromise = new Promise((resolve, reject) => {
                                cpu.usage().then(cpuPercentage => {
                                    //systemInfos['cpuUsage'] = cpuPercentage;
                                    resolve(cpuPercentage)
                                })
                            }).then((obj) => {
                                systemInfos['cpuUsage'] = obj;
                                connection.send(JSON.stringify(systemInfos));
                            });

                        })


                    }, interval)

                    //connection.sendUTF(message.utf8Data);
                } else if (message.type === 'binary') {
                    console.log('Received Binary Message of ' + message.binaryData.length + ' bytes');
                    connection.sendBytes(message.binaryData);
                }
            });

            connection.on('close', function (reasonCode, description) {
                console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
            });
        });

    }
}

const handlePostRequest = async (req, res) => {
    const network = req.body.network;

    if (typeof network !== 'undefined' && network === 'bandwidth') {

        res.json({
            data: 'testasd'
        })

    }
}

exports.systeminformation = systeminformation;