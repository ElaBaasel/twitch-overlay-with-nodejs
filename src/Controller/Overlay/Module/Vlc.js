//const { VLC } = require('node-vlc-http');
const VLC = require("vlc-client");
const net = require("net");

const vlc = async function (req, res, next) {
    const request = req.method;

    if (request === 'POST') await handlePostRequest(req, res);

    if (request === 'GET') {

    }
}

const handlePostRequest = async (req, res) => {
    const vlc = req.body.vlc;

    if (typeof vlc !== 'undefined' && vlc === 'vlc') {

        const client = net.connect(8080, 'localhost', async function () {
            const vlc = new VLC.Client({
                ip: "localhost",
                port: 8080,
                password: "edm"
            });

            let filename = await vlc.getFileName();

            new Promise((resolve, reject) => {
                Promise.resolve(filename).then(function (response) {
                    resolve(response)
                    res.json({
                        data: response
                    })
                });
            });
        });
        client.on('error', function(ex) {
            //TODO Handle Error
        });
    }
}

exports.vlc = vlc;