const path = require("path");
const overlayRepo = require("../../Repository/OverlayRepo");

const overlay = async function (req, res, next) {
    const request = req.method;
    if (request === 'POST') handlePostRequest(req, res);


    if (request === 'GET') {
        res.render(path.join(__dirname + './../../../templates/Overlay/overlay.html.twig'), {
            data: ''
        });

    }
}

const handlePostRequest = async (req, res) => {
    const getOverlay = req.body.getOverlay;

    if (typeof getOverlay !== 'undefined' && getOverlay === 'getOverlay') {
        const getData = await overlayRepo.getData();

        res.json({
            data: getData
        })

    }
}

exports.overlay = overlay;