const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const router = express.Router();
const orm = require("typeorm");
const csrf = require('csurf');
const bodyParser = require('body-parser');

const clientIDSecret = require('./oAuth/ClientIDSecret')
const accessToken = require('./oAuth/AccessToken')

const TwitchConsole = require("./../Entity/TwitchConsole");
const twitchOAuth = require('./../Entity/TwitchOAuthToken')
const TwitchConsoleRepo = require("../Repository/TwitchConsoleRepo");

const csrfProtection = csrf({cookie: true});
const parseForm = bodyParser.urlencoded({extended: false});

router.get('/', csrfProtection, function (req, res, next) {

    res.render(path.join(__dirname+'/../../templates/oAuth.html.twig'), {
        message : "Hello oAuth ",
        csrfToken: req.csrfToken()
    });

});


router.post('/', parseForm, csrfProtection, async function (req, res) {

    let oAuthClientId = req.body.oAuthClientId;
    let oAuthClientSecret = req.body.oAuthClientSecret;
    let buttonName = req.body.buttonNameSave;
    let oAuthShowClientId = req.body.oAuthShowClientId;
    let oAuthShowClientSecret = req.body.oAuthShowClientSecret;
    let oAuthGenerateAccessToken = req.body.oAuthGenerateAccessToken
    let oAuthShowAccessToken = req.body.oAuthShowAccessToken;
    let oAuthToken = req.body.oAuthToken;
    let oAuthHashToken = req.body.oAuthHashToken



    if (typeof buttonName !== 'undefined' && buttonName === 'oAuth[SubmitButton]') {
        await clientIDSecret.submit(oAuthClientId, oAuthClientSecret, res);
    }

    if (typeof oAuthShowClientId !== 'undefined' && oAuthShowClientId === 'oAuth[showClientId]') {
        await clientIDSecret.showClientID(res);
    }

    if (typeof oAuthShowClientSecret !== 'undefined' && oAuthShowClientSecret === 'oAuth[showClientSecret]') {
        await clientIDSecret.showClientSecret(res);
    }

    if (typeof oAuthGenerateAccessToken !== 'undefined' && oAuthGenerateAccessToken === 'twitch_token[generateAccessToken]') {
        let scopeList = req.body.scopeList;
        await accessToken.generate(scopeList, res);
    }

    if (typeof oAuthShowAccessToken !== 'undefined' && oAuthShowAccessToken === 'twitch_token[showAccessToken]') {
        await accessToken.showAccessToken(res);
    }

    if (typeof oAuthToken !== 'undefined' && buttonName === 'twitch_token[tokenSubmitButton]') {
        await accessToken.submit(oAuthToken, res);
    }

    if (typeof buttonName !== 'undefined' && buttonName === 'twitch_token[genUrl]') {

        const twitchConsole = await TwitchConsoleRepo.getData();

        const OAuthRepo = await orm.getRepository(twitchOAuth)
            .createQueryBuilder()
            .getOne();

        const clientId = twitchConsole.client_id;
        const host = 'http://localhost:3000/oAuth';
        const scope = OAuthRepo.scopeList;
        const state = 'b4bc9bb710fb22f804bf03472g113782';

        const url = 'https://id.twitch.tv/oauth2/authorize?response_type=token&client_id='+clientId+'&redirect_uri='+host+'&scope='+scope+'&state='+state;
        return res.json({
            msg: 'oAuthUrl',
            url: url
        })

    }

    if (typeof oAuthHashToken !== 'undefined' && buttonName === 'twitch_token[tokenReturnHashSubmitButton]') {
        await accessToken.submitHashToken(oAuthHashToken, res);
    }
})


module.exports = router;