const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const orm = require("typeorm");
const eventRepo = require("./../../Entity/Event");
const moment = require("moment");
const router = express.Router();


const twitchSigningSecret = 'twitchSigningSecret';


router.get('/', async function (req, res, next) {
    res.render(path.join(__dirname + '/../../../templates/Subevents/hypeTrain.html.twig'), {
        subEvent: 'data',
    });
});

const verifyTwitchSignature = (req, res, buf, encoding) => {
    const messageId = req.header("Twitch-Eventsub-Message-Id");
    const timestamp = req.header("Twitch-Eventsub-Message-Timestamp");
    const messageSignature = req.header("Twitch-Eventsub-Message-Signature");
    const time = Math.floor(new Date().getTime() / 1000);

    if (Math.abs(time - timestamp) > 600) {
        // needs to be < 10 minutes
        console.log(`Verification Failed: timestamp > 10 minutes. Message Id: ${messageId}.`);
        throw new Error("Ignore this request.");
    }

    if (!twitchSigningSecret) {
        console.log(`Twitch signing secret is empty.`);
        throw new Error("Twitch signing secret is empty.");
    }

    const computedSignature =
        "sha256=" +
        crypto
            .createHmac("sha256", twitchSigningSecret)
            .update(messageId + timestamp + buf)
            .digest("hex");
    console.log(`Message ${messageId} Computed Signature: `, computedSignature);

    if (messageSignature !== computedSignature) {
        throw new Error("Invalid signature.");
    } else {
        console.log("Verification successful");
    }
};

router.use(express.json({ verify: verifyTwitchSignature }));

router.post('/', async function (req, res, next) {



    const messageType = req.header("Twitch-Eventsub-Message-Type");


    if (messageType === "webhook_callback_verification") {
        return res.status(200).send(req.body.challenge);
    }

    const { type } = req.body.subscription;
    const { event } = req.body;

    const formatDate = moment(event.started_at).format('YYYY-MM-DD - HH:mm:ss');

    await orm.getConnection()
        .createQueryBuilder()
        .insert()
        .into(eventRepo)
        .values([{
            type: type,
            date: formatDate
        }
        ])
        .execute();

    res.status(200).end();

});



module.exports = router;