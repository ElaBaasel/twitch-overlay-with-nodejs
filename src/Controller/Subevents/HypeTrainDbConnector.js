const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const orm = require("typeorm");
const twitchOAuth = require("./../../Entity/Event");
const router = express.Router();

router.get('/', async function (req, res, next) {

    const getData = await orm.getRepository(twitchOAuth)
        .createQueryBuilder()
        .orderBy('id', 'DESC')
        .getOne();

    res.json({
        date: getData.date,
    })
});


module.exports = router;