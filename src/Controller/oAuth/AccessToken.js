const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const orm = require("typeorm");
const httpReq = require("./../HttpRequest");
const twitchOAuth = require("./../../Entity/TwitchOAuthToken");
const TwitchConsoleRepo = require("../../Repository/TwitchConsoleRepo");

const generate = async (scopeList, response) => {

    const twitchConsole = await TwitchConsoleRepo.getData();

    const clientId = twitchConsole.client_id;
    const secret = twitchConsole.client_secret;

    const token = await httpReq.getOAuthClientCredentialsFlow(clientId, secret, scopeList);

    const oAuthToken = token.data.access_token;
    const expires = token.data.expires_in;

    const getData = await orm.getRepository(twitchOAuth)
        .createQueryBuilder()
        .getOne();


    if (getData) {
        await orm.getConnection()
            .createQueryBuilder()
            .update(twitchOAuth)
            .set({
                token: oAuthToken,
                scopeList: scopeList,
                expires_in: expires
            })
            .where("id = :id", {id: 1})
            .execute();
    }

    if (!getData) {
        await orm.getConnection()
            .createQueryBuilder()
            .insert()
            .into(twitchOAuth)
            .values([{
                token: oAuthToken,
                scopeList: scopeList,
                expires_in: expires
            }
            ])
            .execute();
    }

    return await Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
        class: 'genOAuthToken',
        msg : 'Token was generated and stored in the database'
    }, (err, html) => {
        response.json({
            msg: 'info',
            html: html
        })
    });

}

const showAccessToken = async (response) => {

    const getData = await orm.getRepository(twitchOAuth)
        .createQueryBuilder()
        .getOne();

    return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
        class: 'showOAuthToken',
        msg : getData.token
    }, (err, html) => {
        response.json({
            msg: 'info',
            html: html
        })
    });

}

const submit  = async (oAuthToken, response) => {
    if (!oAuthToken) {

        return Twig.renderFile(path.join(__dirname+'/../../../templates/AlertMsg/InputField.html.twig'), {
            msg : "Empty input field",
            additionalMsg : "Please enter a token",
            class: "alert-warning-token"
        }, (err, html) => {
            res.json({
                msg: 'error',
                html: html
            })
        });
    }

    if (oAuthToken) {
        const getData = await orm.getRepository(twitchOAuth)
            .createQueryBuilder()
            .getOne();


        if (getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .update(twitchOAuth)
                .set({
                    token: oAuthToken,
                    expires_in: ''
                })
                .where("id = :id", {id: 1})
                .execute();
        }

        if (!getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .insert()
                .into(twitchOAuth)
                .values([{
                    token: oAuthToken,
                    expires_in: ''
                }
                ])
                .execute();
        }

        return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
            class: 'token-input',
            msg : 'Input was stored in the database'
        }, (err, html) => {
            response.json({
                msg: 'info',
                html: html
            })
        });

    }

    response.send(true);
}

const submitHashToken = async (oAuthHashToken, response) => {
    if (!oAuthHashToken) {

        return Twig.renderFile(path.join(__dirname+'/../../../templates/AlertMsg/InputField.html.twig'), {
            msg : "Empty input field",
            additionalMsg : "Please enter a token",
            class: "alert-warning-token"
        }, (err, html) => {
            res.json({
                msg: 'error',
                html: html
            })
        });
    }

    if (oAuthHashToken) {
        const getData = await orm.getRepository(twitchOAuth)
            .createQueryBuilder()
            .getOne();


        if (getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .update(twitchOAuth)
                .set({
                    implicitToken: oAuthHashToken,
                    expires_in: ''
                })
                .where("id = :id", {id: 1})
                .execute();
        }

        if (!getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .insert()
                .into(twitchOAuth)
                .values([{
                    implicitToken: oAuthHashToken,
                    expires_in: ''
                }
                ])
                .execute();
        }

        return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
            class: 'token-input',
            msg : 'Input was stored in the database'
        }, (err, html) => {
            response.json({
                msg: 'info',
                html: html
            })
        });

    }

    response.send(true);
}

exports.generate = generate;
exports.showAccessToken = showAccessToken
exports.submit = submit;
exports.submitHashToken = submitHashToken;