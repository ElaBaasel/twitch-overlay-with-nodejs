const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const orm = require("typeorm");
const chatToken = require("./../../Entity/ChatToken");
const chatTokenRepo = require("../../Repository/ChatTokenRepo");

const submit = async (oAuthClientId, oAuthClientSecret, response) => {

    if (!oAuthClientId && !oAuthClientSecret) {

        return Twig.renderFile(path.join(__dirname + '/../../../templates/AlertMsg/InputField.html.twig'), {
            msg: "Empty input field",
            additionalMsg: "Please fill in both or at least 1 input field",
            class: "empty-input"
        }, (err, html) => {
            response.json({
                msg: 'error',
                html: html
            })
        });

    }

    if (!oAuthClientId) {
        oAuthClientId = '';
    }
    if (!oAuthClientSecret) {
        oAuthClientSecret = '';
    }

    const getData = await chatTokenRepo.getData();


    if (getData) {
        await orm.getConnection()
            .createQueryBuilder()
            .update(chatToken)
            .set({
                client_id: oAuthClientId,
                client_secret: oAuthClientSecret
            })
            .where("id = :id", {id: 1})
            .execute();
    }

    if (!getData) {
        await orm.getConnection()
            .createQueryBuilder()
            .insert()
            .into(chatToken)
            .values([{
                client_id: oAuthClientId,
                client_secret: oAuthClientSecret
            }
            ])
            .execute();
    }

    response.send(true);

}

const showClientID = async (response) => {

    const getData = await chatTokenRepo.getData();

    return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
        class: 'showClientId',
        msg : getData.client_id
    }, (err, html) => {
        response.json({
            msg: 'info',
            html: html
        })
    });


}

const showClientSecret = async (response) => {

    const getData = await chatTokenRepo.getData();

    return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
        class: 'showClientSecret',
        msg : getData.client_secret
    }, (err, html) => {
        response.json({
            msg: 'info',
            html: html
        })
    });


}

exports.submit = submit;
exports.showClientSecret = showClientSecret;
exports.showClientID = showClientID;