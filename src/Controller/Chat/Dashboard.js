const chatTokenRepo = require("./../../Repository/ChatTokenRepo");
const path = require("path");
const con = require("./Connect");


const dashboard = async function (req, res, next) {
    const request = req.method;
    if (request === 'POST') handlePostRequest(req, res);


    if (request === 'GET') {
        const chatToken = await chatTokenRepo.getData();

        res.render(path.join(__dirname + './../../../templates/chat.html.twig'), {
            token: chatToken.implicitToken
        });

    }
}

const handlePostRequest = async (req, res) => {
    const buttonNameSave = req.body.buttonNameSave;
    const getChatToken = req.body.getChatToken;

    if (typeof buttonNameSave !== 'undefined' && buttonNameSave === 'chat[connect]') {
        await con.connectBot(req, res);
    }

    if (typeof buttonNameSave !== 'undefined' && buttonNameSave === 'chat[closeConnection]') {
        await con.closeConnection();
    }


    if (typeof getChatToken !== 'undefined' && getChatToken === 'getChatToken') {


    }
}

exports.dashboard = dashboard;