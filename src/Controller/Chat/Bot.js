const tmi = require('tmi.js');
const orm = require("typeorm");
const twitchOAuth = require("./../../Entity/ChatToken");

const bot = async () => {
    const getData = await orm.getRepository(twitchOAuth)
        .createQueryBuilder()
        .getOne();


    const client = new tmi.Client({
        options: { debug: true },
        identity: {
            username: 'elabaaselbot',
            password: 'oauth:'+getData.implicitToken
        },
        channels: [ 'elabaasel' ]
    });

    client.connect();

    client.on('message', (channel, tags, message, self) => {
        // Ignore echoed messages.
        if(self) return;
        client.say(channel, `test`);
        if(message.toLowerCase() === '!hello') {
            // "@alca, heya!"
            client.say(channel, `@${tags.username}, heya!`);
        }
    });


}

exports.bot = bot;

