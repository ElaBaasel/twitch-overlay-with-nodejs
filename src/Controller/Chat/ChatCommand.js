const path = require("path");
const Twig = require("twig");
const chatCommandsRepo = require("../../Repository/ChatCommandsRepo");
const orm = require("typeorm");
const chatCommands = require("./../../Entity/ChatCommands");
const chatToken = require("./../../Entity/ChatToken");

const chatCommand = async function (req, res, next) {
    const request = req.method;
    if (request === 'POST') handlePostRequest(req, res);


    if (request === 'GET') {

        const getData = await chatCommandsRepo.fetchAll();

        res.render(path.join(__dirname + './../../../templates/Chat/chatCommand.html.twig'), {
            commandData: getData
        });

    }
}

const handlePostRequest = async (req, res) => {
    const buttonNameSave = req.body.buttonNameSave;
    const getChatResponse = req.body.chatResponse;
    const getChatCommand = req.body.chatCommand;
    const checkboxId = req.body.checkboxId;
    const isChecked = req.body.isChecked

    if (typeof buttonNameSave !== 'undefined' && buttonNameSave === 'chat[chatCommandSave]') {

        if (!getChatResponse || !getChatCommand) {

            return Twig.renderFile(path.join(__dirname + '/../../../templates/AlertMsg/InputField.html.twig'), {
                msg: "Empty input field",
                additionalMsg: "Please fill in both or at least 1 input field",
                class: "empty-input"
            }, (err, html) => {
                res.json({
                    msg: 'error',
                    html: html
                })
            });
        }

        await orm.getConnection()
            .createQueryBuilder()
            .insert()
            .into(chatCommands)
            .values([{
                enable: 0,
                command: getChatCommand,
                response: getChatResponse
            }
            ])
            .execute();

        return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
            class: 'command-input',
            msg : 'Input was stored in the database'
        }, (err, html) => {
            res.json({
                msg: 'info',
                html: html
            })
        });

    }

    if (buttonNameSave === 'chat[switchCheckBox]') {

        let enable = 0;

        if (isChecked) {
            enable = 1;
        }
        if (!isChecked) {
            enable = 0;
        }

        await orm.getConnection()
            .createQueryBuilder()
            .update(chatCommands)
            .set({
                enable: enable,
            })
            .where("id = :id", {id: checkboxId})
            .execute();

        return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
            class: 'command-active',
            msg : 'Command activated'
        }, (err, html) => {
            res.json({
                msg: 'info',
                html: html
            })
        });


    }

}

exports.chatCommand = chatCommand;