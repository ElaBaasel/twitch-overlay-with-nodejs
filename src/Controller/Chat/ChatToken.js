const path = require("path");
const clientIDSecret = require("./ClientIDSecret");
const accessToken = require("./AccessToken");
const chatTokenRepo = require("./../../Repository/ChatTokenRepo");
const orm = require("typeorm");
const chatToken = require("./../../Entity/ChatToken");

const token = async (req, res, next) => {
    const request = req.method;
    if (request === 'POST') handleChatTokenRequest(req, res);

    if (request === 'GET') {
        res.render(path.join(__dirname+'./../../../templates/Chat/token.html.twig'));
    }
}



const handleChatTokenRequest = async (req, res) => {
    let oAuthClientId = req.body.oAuthClientId;
    let oAuthClientSecret = req.body.oAuthClientSecret;
    let buttonName = req.body.buttonNameSave;
    let oAuthShowClientId = req.body.oAuthShowClientId;
    let oAuthShowClientSecret = req.body.oAuthShowClientSecret;
    let oAuthGenerateAccessToken = req.body.oAuthGenerateAccessToken
    let oAuthShowAccessToken = req.body.oAuthShowAccessToken;
    let oAuthToken = req.body.oAuthToken;
    let oAuthHashToken = req.body.oAuthHashToken


    if (typeof buttonName !== 'undefined' && buttonName === 'oAuth[SubmitButton]') {
        await clientIDSecret.submit(oAuthClientId, oAuthClientSecret, res);
    }

    if (typeof oAuthShowClientId !== 'undefined' && oAuthShowClientId === 'oAuth[showClientId]') {
        await clientIDSecret.showClientID(res);
    }

    if (typeof oAuthShowClientSecret !== 'undefined' && oAuthShowClientSecret === 'oAuth[showClientSecret]') {
        await clientIDSecret.showClientSecret(res);
    }

    if (typeof oAuthGenerateAccessToken !== 'undefined' && oAuthGenerateAccessToken === 'twitch_token[generateAccessToken]') {
        let scopeList = req.body.scopeList;
        await accessToken.generate(scopeList, res);
    }

    if (typeof oAuthShowAccessToken !== 'undefined' && oAuthShowAccessToken === 'twitch_token[showAccessToken]') {
        await accessToken.showAccessToken(res);
    }

    if (typeof oAuthToken !== 'undefined' && buttonName === 'twitch_token[tokenSubmitButton]') {
        await accessToken.submit(oAuthToken, res);
    }

    if (typeof buttonName !== 'undefined' && buttonName === 'twitch_token[genUrl]') {

        const twitchConsole = await chatTokenRepo.getData();

        const OAuthRepo = await orm.getRepository(chatToken)
            .createQueryBuilder()
            .getOne();

        const clientId = twitchConsole.client_id;
        const host = 'http://localhost:3000/chat-token';
        const scope = OAuthRepo.scopeList;
        const state = 'b4bc9bb710fb22f804bf03472g113782';

        const url = 'https://id.twitch.tv/oauth2/authorize?response_type=token&client_id='+clientId+'&redirect_uri='+host+'&scope='+scope+'&state='+state;
        return res.json({
            msg: 'oAuthUrl',
            url: url
        })

    }

    if (typeof oAuthHashToken !== 'undefined' && buttonName === 'twitch_token[tokenReturnHashSubmitButton]') {
        await accessToken.submitHashToken(oAuthHashToken, res);
    }
}

exports.token = token;