const orm = require("typeorm");
const tmi = require("tmi.js");
const twitchOAuth = require("./../../Entity/ChatToken");
const chatCommandsRepo = require("../../Repository/ChatCommandsRepo");
const $ = require( "jquery" );
const overlay = require("./../../Entity/Overlay");

const connectBot = async (req, res) => {
    const getOAuth = await orm.getRepository(twitchOAuth)
        .createQueryBuilder()
        .getOne();

    const client = new tmi.Client({
        options: { debug: true },
        identity: {
            username: 'elabaaselbot',
            password: 'oauth:'+getOAuth.implicitToken
        },
        channels: [ 'elabaasel' ]
    });


    await client.connect();

    client.on('message', async (channel, tags, message, self) => {
        // Ignore echoed messages.
        if (self) return;

        const getData = await chatCommandsRepo.fetchAll();

        Object.keys(getData).forEach(function(key) {

            if (message === getData[key].command && getData[key].enable === 1) {
                client.say(channel, getData[key].response);
            }

        });

        if (message === '!dark') {
            await orm.getConnection()
                .createQueryBuilder()
                .update(overlay)
                .set({
                    layout: 'dark',
                })
                .where("id = :id", {id: 1})
                .execute();
        }
        if (message === '!default') {
            console.log('default layout');
            await orm.getConnection()
                .createQueryBuilder()
                .update(overlay)
                .set({
                    layout: 'default',
                })
                .where("id = :id", {id: 1})
                .execute();
        }
/*        if (message.toLowerCase() === '!hello') {
            // "@alca, heya!"
            client.say(channel, `@${tags.username}, heya!`);
        }

        if (message.toLowerCase() === '!party') {
            client.say(channel, `@${tags.username}, heya!`);
        }

        if (message.toLowerCase() === '!farbe') {
            //TODO: change Color on LEDFX
        }*/
    });

}

const closeConnection = async () => {

}

exports.closeConnection = closeConnection;
exports.connectBot = connectBot;