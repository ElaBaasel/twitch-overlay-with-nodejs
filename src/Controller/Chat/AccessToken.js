const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const orm = require("typeorm");
const httpReq = require("./../HttpRequest");
const chatToken = require("./../../Entity/ChatToken");
const chatTokenRepo = require("../../Repository/ChatTokenRepo");

const generate = async (scopeList, response) => {

    const chatToken = await chatTokenRepo.getData();

    const clientId = chatToken.client_id;
    const secret = chatToken.client_secret;

    const token = await httpReq.getOAuthClientCredentialsFlow(clientId, secret, scopeList);

    const oAuthToken = token.data.access_token;
    const expires = token.data.expires_in;

    const getData = await orm.getRepository(chatToken)
        .createQueryBuilder()
        .getOne();


    if (getData) {
        await orm.getConnection()
            .createQueryBuilder()
            .update(chatToken)
            .set({
                token: oAuthToken,
                scopeList: scopeList,
                expires_in: expires
            })
            .where("id = :id", {id: 1})
            .execute();
    }

    if (!getData) {
        await orm.getConnection()
            .createQueryBuilder()
            .insert()
            .into(chatToken)
            .values([{
                token: oAuthToken,
                scopeList: scopeList,
                expires_in: expires
            }
            ])
            .execute();
    }

    return await Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
        class: 'genOAuthToken',
        msg : 'Token was generated and stored in the database'
    }, (err, html) => {
        response.json({
            msg: 'info',
            html: html
        })
    });

}

const showAccessToken = async (response) => {

    const getData = await orm.getRepository(chatToken)
        .createQueryBuilder()
        .getOne();

    return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
        class: 'showOAuthToken',
        msg : getData.token
    }, (err, html) => {
        response.json({
            msg: 'info',
            html: html
        })
    });

}

const submit  = async (oAuthToken, response) => {
    if (!oAuthToken) {

        return Twig.renderFile(path.join(__dirname+'/../../../templates/AlertMsg/InputField.html.twig'), {
            msg : "Empty input field",
            additionalMsg : "Please enter a token",
            class: "alert-warning-token"
        }, (err, html) => {
            res.json({
                msg: 'error',
                html: html
            })
        });
    }

    if (oAuthToken) {
        const getData = await orm.getRepository(chatToken)
            .createQueryBuilder()
            .getOne();


        if (getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .update(chatToken)
                .set({
                    token: oAuthToken,
                    expires_in: ''
                })
                .where("id = :id", {id: 1})
                .execute();
        }

        if (!getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .insert()
                .into(chatToken)
                .values([{
                    token: oAuthToken,
                    expires_in: ''
                }
                ])
                .execute();
        }

        return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
            class: 'token-input',
            msg : 'Input was stored in the database'
        }, (err, html) => {
            response.json({
                msg: 'info',
                html: html
            })
        });

    }

    response.send(true);
}

const submitHashToken = async (oAuthHashToken, response) => {
    if (!oAuthHashToken) {

        return Twig.renderFile(path.join(__dirname+'/../../../templates/AlertMsg/InputField.html.twig'), {
            msg : "Empty input field",
            additionalMsg : "Please enter a token",
            class: "alert-warning-token"
        }, (err, html) => {
            res.json({
                msg: 'error',
                html: html
            })
        });
    }

    if (oAuthHashToken) {
        const getData = await orm.getRepository(chatToken)
            .createQueryBuilder()
            .getOne();


        if (getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .update(chatToken)
                .set({
                    implicitToken: oAuthHashToken,
                    expires_in: ''
                })
                .where("id = :id", {id: 1})
                .execute();
        }

        if (!getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .insert()
                .into(chatToken)
                .values([{
                    implicitToken: oAuthHashToken,
                    expires_in: ''
                }
                ])
                .execute();
        }

        return Twig.renderFile(path.join(__dirname+'/../../../templates/InfoMsg/InfoPopup.html.twig'), {
            class: 'token-input',
            msg : 'Input was stored in the database'
        }, (err, html) => {
            response.json({
                msg: 'info',
                html: html
            })
        });

    }

    response.send(true);
}

exports.generate = generate;
exports.showAccessToken = showAccessToken
exports.submit = submit;
exports.submitHashToken = submitHashToken;