const axios = require('axios');

const getOAuthClientCredentialsFlow = async (clientId, secret, scope) => {

    return new Promise((resolve, reject) => {

        axios({
            method: 'POST',
            url: 'https://id.twitch.tv/oauth2/token?client_id=' + clientId + '&client_secret=' + secret + '&grant_type=client_credentials&scope=' + scope,

        }).then(function (response) {
            resolve(response);
        })
    });


}

const getSubEventList = async (clientId, token) => {
    return new Promise(async (resolve, reject) => {

        axios({
            headers: {
                'Authorization': 'Bearer ' + token,
                'Client-Id': clientId
            },
            method: 'GET',
            url: 'https://api.twitch.tv/helix/eventsub/subscriptions',

        }).then(function (response) {
            resolve(response);
        })
    });
}


const getUserId = async (token, clientId, userName) => {
    return new Promise(async (resolve, reject) => {

        axios({
            headers: {
                'Authorization': 'Bearer ' + token,
                'Client-Id': clientId
            },
            method: 'GET',
            url: 'https://api.twitch.tv/helix/users?login='+userName,

        }).then(function (response) {
            resolve(response);
        })

    })
}

const deleteEventSubscriptions = async (token, clientId, eventId) => {
    return new Promise(async (resolve, reject) => {

        axios({
            headers: {
                'Authorization': 'Bearer ' + token,
                'Client-Id': clientId
            },
            method: 'DELETE',
            url: 'https://api.twitch.tv/helix/eventsub/subscriptions?id='+eventId,

        }).then(function (response) {
            resolve(response);
        })

    })
}

const submitSubEvent = async (token, clientId, jsonData) => {
    return new Promise(async (resolve, reject) => {

        axios({
            headers: {
                'Authorization': 'Bearer ' + oAuth.token,
                'Client-Id': twitchConsole.client_id,
                'Content-Type': 'application/json'
            },
            method: 'POST',
            url: 'https://api.twitch.tv/helix/eventsub/subscriptions',
            data: jsonData

        }).then(function (response) {
            resolve(response);
        }).catch(error => {
            if (typeof error.response.data.error != 'undefined') {
                resolve(error.response.data);
                //return error.response.data
            }
        });
    })
}

const getUserFollows = async (token, clientId, userId) => {
    return new Promise(async (resolve, reject) => {
        axios({
            headers: {
                'Authorization': 'Bearer ' + token,
                'Client-Id': clientId,
            },
            method: 'GET',
            url: 'https://api.twitch.tv/helix/users/follows?to_id='+userId+'&first=100'
        }).then(function (response) {
            resolve(response);
        }).catch(error => {
            if (typeof error.response.data.error != 'undefined') {
                resolve(error.response.data);
                //return error.response.data
            }
        });

    });
}

exports.getOAuthClientCredentialsFlow = getOAuthClientCredentialsFlow;
exports.getSubEventList = getSubEventList;
exports.getUserId = getUserId;
exports.deleteEventSubscriptions = deleteEventSubscriptions;
exports.submitSubEvent = submitSubEvent;
exports.getUserFollows = getUserFollows