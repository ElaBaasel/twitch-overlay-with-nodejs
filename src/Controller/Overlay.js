
const express = require('express');
const router = express.Router();


const {overlay} = require('./Overlay/Overlay');
const {bandwidth} = require('./Overlay/Module/Bandwidth');
const {systeminformation} = require('./Overlay/Module/Systeminformation');
const {vlc} = require('./Overlay/Module/Vlc');
//const {overlayLight} = require("./Overlay/Overlay-Light");
//const {overlayDark} = require("./Overlay/Overlay-Dark");

const index = () => {
    router.all('/overlay', [overlay]);
    router.all('/overlay/module/bandwidth', [bandwidth]);
    router.all('/overlay/module/systeminformation', [systeminformation]);
    router.all('/overlay/module/vlc', [vlc]);
    //router.all('/overlay-light', [overlayLight]);
    //router.all('/overlay-dark', [overlayDark]);
    return module.exports = router;
}

exports.index = index;

