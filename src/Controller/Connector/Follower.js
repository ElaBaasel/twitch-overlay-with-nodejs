const path = require("path");
const overlayRepo = require("../../Repository/OverlayRepo");
const httpReq = require("./../HttpRequest");
const TwitchConsoleRepo = require("./../../Repository/TwitchConsoleRepo");
const orm = require("typeorm");
const twitchOAuth = require("./../../Entity/TwitchOAuthToken");
const followEntity = require("./../../Entity/Follower");
const followers = require("./../../Repository/Followers");

const follower = async function (req, res, next) {
    const request = req.method;
    if (request === 'POST') handlePostRequest(req, res);


    if (request === 'GET') {
        res.render(path.join(__dirname + './../../../templates/Overlay/overlay.html.twig'), {
            data: ''
        });

    }
}

const handlePostRequest = async (req, res) => {
    const follower = req.body.follower;
    const connector = req.body.connector;

    if (typeof connector !== 'undefined' && connector === 'connector') {

        const twitchConsole = await TwitchConsoleRepo.getData();
        const oAuth = await orm.getRepository(twitchOAuth)
            .createQueryBuilder()
            .getOne();

        const getUserFollows = await httpReq.getUserFollows(oAuth.token, twitchConsole.client_id, twitchConsole.user_id);

        const getData = await orm.getRepository(followEntity)
            .createQueryBuilder()
            .getOne();


        if (getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .update(followEntity)
                .set({
                    data: JSON.stringify(getUserFollows.data),
                })
                .where("id = :id", { id: 1 })
                .execute();
        }

        if (!getData) {
            await orm.getConnection()
                .createQueryBuilder()
                .insert()
                .into(followEntity)
                .values([{
                    data: JSON.stringify(getUserFollows.data),
                }
                ])
                .execute();
        }

        res.json({
            data: getUserFollows.data,
        })
    }

    if (typeof follower !== 'undefined' && follower === 'follower') {
        //console.log('follower');
        const getData = await followers.getData();

        res.json({
            data: getData
        })

    }
}

exports.follower = follower;