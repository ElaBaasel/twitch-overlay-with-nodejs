const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const router = express.Router();

const chat = require('./Chat/Bot');
const {dashboard} = require("./Chat/Dashboard");
const {token} = require("./Chat/ChatToken");
const {chatCommand} = require("./Chat/ChatCommand");

const index = () => {
    router.all('/chat', [dashboard]);
    router.all('/chat-token', [token]);
    router.all('/chat-command', [chatCommand]);
    router.all('/handle-nanoleaf', [token]);
    router.all('/handle-ledFX', [token]);
    return module.exports = router;
}


const connect = async () => {
    router.get('/', async function (req, res, next) {
        await chat.bot();
    });
    return module.exports = router;
}

exports.index = index;
exports.connect = connect;