const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const router = express.Router();


const {follower} = require('./Connector/Follower');

const index = () => {
    router.all('/connector', [follower]);
    return module.exports = router;
}

exports.index = index;

