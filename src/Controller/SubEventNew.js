const path = require('path');
const Twig = require('twig'),
    express = require('express'),
    app = express();
const router = express.Router();
const orm = require("typeorm");
const httpReq = require('./HttpRequest');
const TwitchConsoleRepo = require("../Repository/TwitchConsoleRepo");
const Tunnel = require("./../Entity/Tunnel");
const Cryptr = require("cryptr");
const twitchOAuth = require("./../Entity/TwitchOAuthToken");


router.get('/', async function (req, res, next) {

    res.render(path.join(__dirname + '/../../templates/subEventNew.html.twig'), {
        subEvent: 'data',
    });

});

router.post('/', async function (req, res) {

    let buttonNameSave = req.body.buttonNameSave;

    let type = req.body.type;
    let version = req.body.version;
    let broadcasterUserId = req.body.broadcasterUserId;
    let method = req.body.method;
    let callback = req.body.callback
    let secret = req.body.secret;
    let saveAsTemplate = req.body.saveAsTemplate;
    let autoStart = req.body.autoStart;

    if (typeof buttonNameSave !== 'undefined' && buttonNameSave === 'subevent[getUserId]') {
        const twitchConsole = await TwitchConsoleRepo.getData();

        return res.json({
            msg: 'userId',
            html: twitchConsole.user_id
        })
    }

    if (typeof buttonNameSave !== 'undefined' && buttonNameSave === 'subevent[getTunnelUrl]') {
        const tunnel = await orm.getRepository(Tunnel)
            .createQueryBuilder()
            .getOne();

        const cryptr = new Cryptr(process.env.TUNNELPASS);

        return res.json({
            msg: 'tunnelUrl',
            html: cryptr.decrypt(tunnel.url)
        })
    }

    if (typeof buttonNameSave !== 'undefined' && buttonNameSave === 'subevent[save]') {

        if (autoStart) {
            //add as autostart
        }
        if (saveAsTemplate) {
            //save as template
        }

        const body = {};
        body['type'] = type
        body['version'] = version
        body['condition'] = {}
        body['condition']['broadcaster_user_id'] = broadcasterUserId
        body['transport'] = {}
        body['transport']['method'] = method
        body['transport']['callback'] = callback
        body['transport']['secret'] = secret

        const oAuth = await orm.getRepository(twitchOAuth)
            .createQueryBuilder()
            .getOne();

        const twitchConsole = await TwitchConsoleRepo.getData();

        const responseData = await httpReq.submitSubEvent(oAuth.token, twitchConsole.client_id, body);

        if (typeof responseData.error !== 'undefined') {

            return Twig.renderFile(path.join(__dirname+'/../../templates/AlertMsg/InputField.html.twig'), {
                msg : responseData.error+' - '+responseData.status,
                additionalMsg : responseData.message,
                class: "empty-input"
            }, (err, html) => {
                res.json({
                    msg: 'error',
                    html: html
                })
            });
        }

        if (typeof responseData.data.total !== 'undefined') {
            return Twig.renderFile(path.join(__dirname+'/../../templates/InfoMsg/InfoPopup.html.twig'), {
                class: 'addSubevent',
                msg : 'Subevent successfully added'
            }, (err, html) => {
                res.json({
                    msg: 'info',
                    html: html
                })
            });
        }

    }

});

module.exports = router;

