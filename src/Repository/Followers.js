const orm = require("typeorm");
const follower = require("./../Entity/Follower");

const getData = async () => {
    return await orm.getRepository(follower)
        .createQueryBuilder()
        .getOne();
}

exports.getData = getData