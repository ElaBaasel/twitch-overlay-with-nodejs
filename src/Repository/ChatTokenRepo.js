const orm = require("typeorm");
const chatToken = require("./../Entity/ChatToken");

const getData = async () => {
    return await orm.getRepository(chatToken)
        .createQueryBuilder()
        .getOne();
}

exports.getData = getData