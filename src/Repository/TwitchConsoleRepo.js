const orm = require("typeorm");
const TwitchConsole = require("./../Entity/TwitchConsole");

const getData = async () => {
    return await orm.getRepository(TwitchConsole)
        .createQueryBuilder()
        .getOne();
}

exports.getData = getData