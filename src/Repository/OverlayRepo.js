const orm = require("typeorm");
const overlay = require("./../Entity/Overlay");

const getData = async () => {
    return await orm.getRepository(overlay)
        .createQueryBuilder()
        .getOne();
}

exports.getData = getData