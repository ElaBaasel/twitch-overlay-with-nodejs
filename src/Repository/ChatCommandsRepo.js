const orm = require("typeorm");
const chatCommands = require("./../Entity/ChatCommands");

const getData = async () => {
    return await orm.getRepository(chatCommands)
        .createQueryBuilder()
        .getOne();
}

const fetchAll = async () => {
    return await orm.getRepository(chatCommands)
        .createQueryBuilder()
        .getMany();
}
exports.fetchAll = fetchAll;
exports.getData = getData