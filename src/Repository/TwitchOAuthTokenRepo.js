const orm = require("typeorm");
const TwitchOAuth = require("./../Entity/TwitchOAuthToken");

const getData = async () => {
    return await orm.getRepository(TwitchOAuth)
        .createQueryBuilder()
        .getOne();
}

exports.getData = getData