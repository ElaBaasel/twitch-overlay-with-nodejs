const EntitySchema = require("typeorm").EntitySchema;

module.exports = new EntitySchema({
    name: "Follower",
    tableName: "follower",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        data: {
            type: "text",
            nullable: true
        }
    }
});