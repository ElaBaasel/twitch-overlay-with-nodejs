const EntitySchema = require("typeorm").EntitySchema;

module.exports = new EntitySchema({
    name: "ChatToken",
    tableName: "chatToken",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        client_id: {
            type: "varchar",
            nullable: true
        },
        client_secret: {
            type: "varchar",
            nullable: true
        },
        user_id: {
            type: "int",
            nullable: true
        },
        username: {
            type: "varchar",
            nullable: true
        },
        token: {
            type: "varchar",
            nullable: true
        },
        implicitToken: {
            type: "varchar",
            nullable: true
        },
        scopeList: {
            type: "varchar",
            nullable: true
        },
        expires_in: {
            type: "int",
            nullable: true
        }
    }
});