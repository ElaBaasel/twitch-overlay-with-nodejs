const EntitySchema = require("typeorm").EntitySchema;

module.exports = new EntitySchema({
    name: "twitchConsole",
    tableName: "twitchconsole",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        client_id: {
            type: "varchar",
            nullable: true
        },
        client_secret: {
            type: "varchar",
            nullable: true
        },
        user_id: {
            type: "int",
            nullable: true
        },
        username: {
            type: "varchar",
            nullable: true
        }
    }
});