const EntitySchema = require("typeorm").EntitySchema;

module.exports = new EntitySchema({
    name: "Twitch_oauth",
    tableName: "twitch_oauth",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        token: {
            type: "varchar"
        },
        implicitToken: {
            type: "varchar",
            nullable: true
        },
        scopeList: {
            type: "varchar"
        },
        expires_in: {
            type: "int"
        }
    }
});