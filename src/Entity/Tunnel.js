const EntitySchema = require("typeorm").EntitySchema;

module.exports = new EntitySchema({
    name: "Tunnel",
    tableName: "Tunnel",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        url: {
            type: "text"
        }
    }
});