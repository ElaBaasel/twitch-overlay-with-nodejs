const EntitySchema = require("typeorm").EntitySchema;

module.exports = new EntitySchema({
    name: "Overlay",
    tableName: "overlay",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        layout: {
            type: "varchar",
            nullable: true
        },
        active: {
            type: "int",
            nullable: true
        }
    }
});