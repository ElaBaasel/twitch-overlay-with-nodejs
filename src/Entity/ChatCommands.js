const EntitySchema = require("typeorm").EntitySchema;

module.exports = new EntitySchema({
    name: "ChatCommands",
    tableName: "chatcommands",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        enable: {
            type: "int",
            nullable: true
        },
        command: {
            type: "varchar",
            nullable: true
        },
        response: {
            type: "varchar",
            nullable: true
        }
    }
});