const EntitySchema = require("typeorm").EntitySchema;

module.exports = new EntitySchema({
    name: "Event",
    tableName: "event",
    columns: {
        id: {
            primary: true,
            type: "int",
            generated: true
        },
        type: {
            type: "varchar"
        },
        date: {
            type: "text"
        }
    }
});