### TwitchApp

-----
Dependencies:
- download and install nodejs (`https://nodejs.org/en/`)
- type `npm install` to install and wait if ready
- type `npm run start` to start the app
- add `.env` file
```
PORT=3000
TUNNELPASS=>>SECRET_PASSWORD<<
```
Note:
- no database Server installation required because runs with sqlite

-----

### For devloping
- run `yarn encore dev --watch` to compile assets files in /assets. Defined in webpack.config.js