// import style libs
import 'bootstrap';
import '@fortawesome/fontawesome-free/css/all.css';

// import self coded
import './css/index.scss';

// import js
import './bootstrap';

// import js libs
import $ from 'jquery';
global.$ = global.jQuery = $;