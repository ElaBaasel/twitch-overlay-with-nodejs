import './../css/Overlay/Default/hexagon.css';

// import js libs
import $ from 'jquery';
global.$ = global.jQuery = $;


import { startStimulusApp } from '@symfony/stimulus-bridge';
import overlay from './../Controller/Overlay';

// Registers Stimulus controllers from controllers.json and in the controllers/ directory
export const app = startStimulusApp(require.context(
    '@symfony/stimulus-bridge/lazy-controller-loader!./../Controller',
    true,
    /\.(j|t)sx?$/
));



app.register('overlay', overlay);
