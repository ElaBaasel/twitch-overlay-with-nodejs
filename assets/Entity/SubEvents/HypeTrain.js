import { startStimulusApp } from '@symfony/stimulus-bridge';
import hypeTrain from './../../Controller/SubEvents/HypeTrain';

// Registers Stimulus controllers from controllers.json and in the controllers/ directory
export const app = startStimulusApp(require.context(
    '@symfony/stimulus-bridge/lazy-controller-loader!./../../Controller/SubEvents',
    true,
    /\.(j|t)sx?$/
));

app.register('hypeTrain', hypeTrain);