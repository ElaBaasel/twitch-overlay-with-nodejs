import {Controller} from "stimulus"
import axios from "axios";
import $ from "jquery";

export default class extends Controller {

    connect() {

        let url = this.element.dataset.urlSubevent;

        const $table = $('.subscriptionsTable');
        const $delButton = $table.find('.dropdown-item');
        const $secretUrl = $table.find('.secretUrl');
        $($secretUrl).click(function (ev) {
            const secret = $(this).attr('data-secretUrl');

            axios({
                data: {
                    secret: secret
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                if (res.msg === 'encodedUrl') {
                    const encodedUrl = res.url
                    $(ev.target).text(encodedUrl);

                }

            });
        });

        $($delButton).click(function (ev) {
            const eventId = $(this).attr('data-sub-id');

            axios({
                data: {
                    eventId: eventId
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'reloadPage') {
                    location.reload();
                }
            });

        });

    }
}
