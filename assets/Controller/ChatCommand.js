import {Controller} from "stimulus"
import axios from "axios";
import $ from "jquery";

export default class extends Controller {

    connect() {

        let url = this.element.dataset.urlChatcommand;


        $('#chatCommandSave').click(function (e) {
            let chatCommand = document.getElementById("chatCommand").value;
            let chatResponse = document.getElementById("chatResponse").value;
            let buttonNameSave = this.name;

            axios({
                data: {
                    chatCommand: chatCommand,
                    chatResponse: chatResponse,
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'error') {
                    $('.empty-input').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }

                if (res.msg === 'info') {
                    document.getElementById("chatCommand").value = '';
                    document.getElementById("chatResponse").value = '';

                    $('.command-input').remove();
                    $('.toastBox').prepend(res.html);
                    const toastMsg = document.getElementById("toastMsg");
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                    location.reload();
                }

            });
        });

        $('[id^=switchCheckChatCommand]').click(function(e){
            const checkboxId = $(this).attr('data-checkbox-id');
            const buttonNameSave = this.name;
            const isChecked = e.target.checked;


            axios({
                data: {
                    checkboxId: checkboxId,
                    isChecked: isChecked,
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'info') {

                    $('.command-active').remove();
                    $('.toastBox').prepend(res.html);
                    const toastMsg = document.getElementById("toastMsg");
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }

            });

        });

    }
}