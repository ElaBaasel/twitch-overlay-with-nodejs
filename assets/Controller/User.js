import {Controller} from "stimulus"
import axios from "axios";
import $ from "jquery";

export default class extends Controller {

    connect(){

        let url = this.element.dataset.urlUser;
        let CSRFToken = document.getElementById("csrfToken").value;

        $('#userNameSubmitButton').click(function (e) {

            let userName = document.getElementById("userName").value;
            let buttonNameSave = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    userName: userName,
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'info') {
                    $('.showUserId').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()

                }
                if (res.msg === 'error') {
                    $('.empty-input-userName').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()

                }
            });

        });

        $('#userIdSubmitButton').click(function (e) {

            let userId = document.getElementById("userid").value;
            let buttonNameSave = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    userId: userId,
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'info') {
                    $('.storeUserId').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }
                if (res.msg === 'error') {
                    $('.empty-input-userId').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }
            });

        });


        $('#submitSaveUsername').click(function (e) {

            let userName = document.getElementById("saveUsername").value;
            let buttonNameSave = this.name;
            console.log(userName);
            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    saveUserName: userName,
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'info') {
                    $('.storeUserId').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }
                if (res.msg === 'error') {
                    $('.empty-input-userId').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }
            });

        });

    }
}