import {Controller} from "stimulus"
import axios from "axios";
import $ from "jquery";

export default class extends Controller {

    connect() {

        let url = this.element.dataset.urlSubevent;

        $('#subeventOpenTemplate').click(function (e) {
            let buttonNameSave = this.name;
        });

        $('#getUserId').click(function (ev) {
            const buttonNameSave = this.name;
            axios({
                data: {
                    buttonNameSave: buttonNameSave,
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                $(ev.target).closest('.input-group').find('input').val(res.html)
            });
        });

        $('#getTunnelUrl').click(function (ev) {
            const buttonNameSave = this.name;
            axios({
                data: {
                    buttonNameSave: buttonNameSave,
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                $(ev.target).closest('.input-group').find('input').val(res.html)
            });
        });

        $('#subeventSubmitButton').click(function (e) {
            const buttonNameSave = this.name;

            const saveAsTemplate = document.getElementById("saveAsTemplate").checked;
            const autoStart = document.getElementById("autoStart").checked;

            const type = document.getElementById("type").value;
            const version = document.getElementById("version").value;
            const broadcasterUserId = document.getElementById("broadcasterUserId").value;
            const method = document.getElementById("method").value;
            const callback = document.getElementById("callback").value;
            const secret = document.getElementById("secret").value;

            axios({
                data: {
                    buttonNameSave: buttonNameSave,
                    type: type,
                    version: version,
                    broadcasterUserId: broadcasterUserId,
                    method: method,
                    callback: callback,
                    secret: secret,
                    saveAsTemplate: saveAsTemplate,
                    autoStart: autoStart
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'error') {
                    $('.empty-input').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()

                }
                if (res.msg === 'info') {
                    $('.addSubevent').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()

                }
            });

        });
    }
}