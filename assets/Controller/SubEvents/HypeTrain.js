import {Controller} from "stimulus"
import axios from "axios";
import $ from "jquery";
import moment from "moment";

export default class extends Controller {

    connect(){

        let currentTimeStamp = Date.now();

        let returnValue = this.index();
        new Promise((resolve, reject) => {
            Promise.resolve(returnValue).then(function (response) {
                resolve(response);
                const dbTime = response.data.date;
                const regexTime = dbTime.replace(/( - )/g, ' ');
                currentTimeStamp = moment(regexTime).format('x');
            });
        });

        setInterval(() => {
            let ret = this.index();
            new Promise((resolve, reject) => {
                Promise.resolve(ret).then(function (response) {
                    resolve(response);
                    const dbTime = response.data.date;
                    const regexTime = dbTime.replace(/( - )/g, ' ');
                    const databaseTimeStamp = moment(regexTime).format('x');

                    if (databaseTimeStamp !== currentTimeStamp) {
                        currentTimeStamp = databaseTimeStamp;

                        $('#hypeTrain').show();
                        setTimeout(function() {
                            $('#hypeTrain').hide();
                        }, 5000);

                    }
                });
            });
        }, 500);
    }


    index() {

        let requestToServer = this.apiCall();

        return new Promise((resolve, reject) => {
            Promise.resolve(requestToServer).then(function (response) {
                resolve(response);
            });
        });
    }

    apiCall() {

        return new Promise((resolve, reject) => {
            axios({
                method: 'get',
                url: this.element.dataset.urlSubevent,
                responseType: 'json'
            }).then(function (response) {
                resolve(response);
            })
        });

    }
}