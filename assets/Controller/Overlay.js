import {Controller} from "stimulus"

import {animateName} from "./Overlay/Default/animateName"
import {bgLayoutTL} from "./Overlay/Default/bgLayoutTL"
import {bgLayoutTR} from "./Overlay/Default/bgLayoutTR"
import {bgLayoutBR} from "./Overlay/Default/bgLayoutBR"
import {latest5follower} from "./Overlay/Modul/latest5follower/index"
import {newFollower} from "./Overlay/Modul/newFollower/index"
import {Network} from "./Overlay/Modul/Network";
import {Systeminformation} from "./Overlay/Modul/Systeminformation";
import {Vlc} from "./Overlay/Modul/Vlc";
import {last5follower} from "./Overlay/Default/last5follower"

import {bgLayoutTLDark} from "./Overlay/Dark/bgLayoutTL"
import {bgLayoutTRDark} from "./Overlay/Dark/bgLayoutTR"
import {bgLayoutBRDark} from "./Overlay/Dark/bgLayoutBR"



import axios from "axios";
import moment from "moment";
import $ from "jquery";
import {play} from "./Overlay/Playground/play";


export default class extends Controller {

    connect() {

        let url = this.element.dataset.urlOverlay;

        let currentLayout = '';

        setInterval(() => {
            let ret = this.index();
            new Promise((resolve, reject) => {
                Promise.resolve(ret).then(function (response) {
                    resolve(response);
                    const data = response.data.data;

                    if (data.layout !== currentLayout) {

                        currentLayout = data.layout;
                        $('canvas').remove();
                        if (data.layout === 'default') {
                            console.log('loading Default');
                            //$('body').append('<canvas id="root">');
                            animateName();
                            bgLayoutTL();
                            bgLayoutTR();
                            bgLayoutBR();
                            bgLayoutBR();
                            new latest5follower();
                            new newFollower();
                            new Network();
                            //new Systeminformation();
                            new Vlc();

                        }

                        if (data.layout === 'dark') {
                            console.log('loading Dark');
                            bgLayoutTLDark();
                            bgLayoutTRDark();
                            bgLayoutBRDark();
                        }
                    }
                });
            });

        }, 1000);

        setInterval(() => {
            let ret = this.indexRes();

            new Promise((resolve, reject) => {
                Promise.resolve(ret).then(function (response) {
                    resolve(response);
                });
            });


        }, 5000)


    }


    index() {

        let requestToServer = this.apiCall();

        return new Promise((resolve, reject) => {
            Promise.resolve(requestToServer).then(function (response) {
                resolve(response);
            });
        });
    }

    indexRes() {
        let requestToServer = this.connectorCall();

        return new Promise((resolve, reject) => {
            Promise.resolve(requestToServer).then(function (response) {
                resolve(response);
            });
        });
    }

    apiCall() {

        return new Promise((resolve, reject) => {
            axios({
                method: 'POST',
                data: {
                    getOverlay: 'getOverlay'
                },
                url: this.element.dataset.urlOverlay,
                responseType: 'json'
            }).then(function (response) {
                resolve(response);
            })
        });
    }

    connectorCall() {
        return new Promise((resolve, reject) => {
            axios({
                method: 'POST',
                data: {
                    connector: 'connector'
                },
                url: '/connector',
                responseType: 'json'
            }).then(function (response) {
                resolve(response);

            });
        });
    }
}