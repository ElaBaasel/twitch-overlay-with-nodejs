import axios from "axios";
import * as THREE from "three";
import {Camera} from "./Camera";
import {Scene} from "./Scene";
import {HexagonOverlay} from "./HexagonOverlay";
import * as TWEEN from "@tweenjs/tween.js";
import {FontLoader} from "three/examples/jsm/loaders/FontLoader";
import Utility from "../../Default/Utility";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";

class Network
{
    constructor() {
        this.loadWebsocket = 1;
        this.networkIO();
    }

    startWebsocket() {
        axios({
            method: 'GET',
            params: {
                interval: 2000
            },
            url: '/overlay/module/bandwidth',
            responseType: 'json'
        });
    }

    async networkIO() {

        let renderer;

        const camera = await new Camera(THREE);
        const scene = await new Scene(THREE);

        let utility = new Utility(camera);

        const hexagonOverlay = new HexagonOverlay(THREE, scene, camera, utility)
        const groupMatLineHex = hexagonOverlay.displayHexagon();

        const loader = new FontLoader();
        if (this.loadWebsocket === 0) {
            loader.load('fonts/Roboto_Medium_Regular.json', async function (fontResponse) {

                const matLite = new THREE.MeshBasicMaterial({
                    color: 0xFFFFFF,
                    transparent: true,
                    depthWrite: false,
                    opacity: 1,
                    side: THREE.DoubleSide,
                    polygonOffset: true,
                    polygonOffsetFactor: 1, // positive value pushes polygon further away
                    polygonOffsetUnits: 1
                });

                const incomingText = '0.25 mb/s'

                const shapesIncoming = fontResponse.generateShapes(incomingText, 16);

                const geometryIncoming = new THREE.ShapeGeometry(shapesIncoming);


                let textIncoming = new THREE.Mesh(geometryIncoming, matLite);

                let boundingBoxIncoming = new THREE.Box3().setFromObject(textIncoming)

                textIncoming.position.x = -(boundingBoxIncoming.max.x / 2);
                textIncoming.position.y = -30;
                textIncoming.position.z = 10;
                textIncoming.name = 'network'
                scene.add(textIncoming);

                const outgoingText = '30,25 mb/s'
                const shapesOutgoing = fontResponse.generateShapes(outgoingText, 16);

                const geometryOutgoing = new THREE.ShapeGeometry(shapesOutgoing);

                //let boundingOutgoing = geometryOutgoing.computeBoundingBox();

                let textOutgoing = new THREE.Mesh(geometryOutgoing, matLite);

                let boundingBoxOutgoing = new THREE.Box3().setFromObject(textOutgoing)

                textOutgoing.position.x = -(boundingBoxOutgoing.max.x / 2)+130;
                textOutgoing.position.y = 40;
                textOutgoing.position.z = 10;
                textOutgoing.name = 'network'


                scene.add(textOutgoing);

            });
        }
        if (this.loadWebsocket === 1) {

            this.startWebsocket();
            let connection = new WebSocket('ws://localhost:6080/', 'echo-protocol');

            connection.onopen = function () {
                connection.send('Ping'); // Send the message 'Ping' to the server
            };

            // Log errors
            connection.onerror = function (error) {
                console.log('WebSocket Error ' + error);
            };


            loader.load('fonts/Roboto_Medium_Regular.json', async function (fontResponse) {

            const matLite = new THREE.MeshBasicMaterial({
                color: 0xFFFFFF,
                transparent: true,
                depthWrite: false,
                opacity: 1,
                side: THREE.DoubleSide,
                polygonOffset: true,
                polygonOffsetFactor: 1, // positive value pushes polygon further away
                polygonOffsetUnits: 1
            });


            connection.onmessage = function (e) {

                let data = JSON.parse(e.data);

                let objectsToRemove = [];
                scene.traverse(function(object) {
                    if (object.isMesh && object.name === 'network') {
                        objectsToRemove.push(object);
                    }
                });

                objectsToRemove.forEach(node => {
                    let parent = node.parent;
                    parent.remove( node );
                });

                const incomingMb = data.incomingMb.toString();
                const outgoingMb = data.outgoingMb.toString();

                const incomingText = incomingMb+' mb/s'

                const shapesIncoming = fontResponse.generateShapes(incomingText, 16);
                const geometryIncoming = new THREE.ShapeGeometry(shapesIncoming);
                let textIncoming = new THREE.Mesh(geometryIncoming, matLite);
                let boundingBoxIncoming = new THREE.Box3().setFromObject(textIncoming)

                textIncoming.position.x = -(boundingBoxIncoming.max.x / 2);
                textIncoming.position.y = -30;
                textIncoming.position.z = 10;
                textIncoming.name = 'network'
                textIncoming.content = incomingMb;
                scene.add(textIncoming);




                const shapesOutgoing = fontResponse.generateShapes(outgoingMb+' mb/s', 16);
                const geometryOutgoing = new THREE.ShapeGeometry(shapesOutgoing);
                let textOutgoing = new THREE.Mesh(geometryOutgoing, matLite);
                let boundingBoxOutgoing = new THREE.Box3().setFromObject(textOutgoing)
                textOutgoing.position.x = -(boundingBoxOutgoing.max.x / 2)+130;
                textOutgoing.position.y = 40;
                textOutgoing.position.z = 10;
                textOutgoing.name = 'network'
                scene.add(textOutgoing);
            }
        });
        }
        // RENDERER
        renderer = new THREE.WebGLRenderer( {
            alpha: true,
            antialias: true
        } );
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
        renderer.setPixelRatio( 1 );
        renderer.setSize( 1920, 1080 );
        renderer.setClearColor(0x000000, 0);

        const controls = new OrbitControls(camera, renderer.domElement);

        document.body.appendChild( renderer.domElement );


        const animate = function (time) {
            requestAnimationFrame( animate );
            TWEEN.update(time)

            renderer.setViewport( -880, -450, 1920, 1080 );
            renderer.domElement.id = 'network';
            renderer.domElement.style.zIndex = "20";
            render();
        };

        const render = function () {
            renderer.autoClear = false;


            let i;
            for (i = 0; i < groupMatLineHex.length; i++) {
                groupMatLineHex[i].resolution.set( 1920, 1080 );
            }

            renderer.clear();
            renderer.render(scene, camera);
        };

        const update = function () {

        }
        animate();

    }
}

export {Network}