import * as THREE from "three";
import {FontLoader} from "three/examples/jsm/loaders/FontLoader";

class NetworkHexagon
{

    constructor(THREE, scene, camera, utility) {
        this.THREE = THREE;
        this.scene = scene;
        this.camera = camera;
        this.utility = utility;
    }

    setNetworkHexagon()
    {
        const THREE = this.THREE
        const scene = this.scene
        const utility = this.utility;
        const camera = this. camera;

        let margin = 1.1;
        let scale = 80;

        let groupMatLineHex = [];

        //HeadlineText
        let borderHex1 = utility.addBorderLine(0x93278f);
        borderHex1.matLine.linewidth = 4;
        groupMatLineHex.push(borderHex1.matLine);
        let geoAttributeHex1 = utility.setGeoAttribute(borderHex1.matLine, borderHex1);

        //upload
        let borderHex2 = utility.addBorderLine(0x29abe2);
        borderHex2.matLine.linewidth = 4;
        groupMatLineHex.push(borderHex2.matLine);
        let geoAttributeHex2 = utility.setGeoAttribute(borderHex2.matLine, borderHex2);

        //download
        let borderHex3 = utility.addBorderLine(0x29abe2);
        borderHex3.matLine.linewidth = 4;
        groupMatLineHex.push(borderHex3.matLine);
        let geoAttributeHex3 = utility.setGeoAttribute(borderHex3.matLine, borderHex3);

        const lineWidth = 1000/camera.position.z*utility.lineMultiplier;
        const scaleSet = (camera.position.z/1000)*lineWidth*250;

        //HeadlineText
        borderHex1.line.scale.set(61, 61, 0);
        borderHex1.line.position.set(10*margin, 100*margin, 0);
        borderHex1.line.rotateZ(THREE.Math.degToRad(30));
        borderHex1.matLine.opacity = 1;

        //upload
        borderHex2.line.scale.set(80, 80, 0);
        borderHex2.line.position.set(120*margin, 50, 0);
        borderHex2.line.rotateZ(THREE.Math.degToRad(30));
        borderHex2.matLine.opacity = 1;

        //download
        borderHex3.line.scale.set(80, 80, 0);
        borderHex3.line.position.set(0, -20*margin, 0);
        borderHex3.line.rotateZ(THREE.Math.degToRad(30));
        borderHex3.matLine.opacity = 1;

        utility.addToScene(scene, [
            borderHex1.line,
            geoAttributeHex1
        ]);
        utility.addToScene(scene, [
            borderHex2.line,
            geoAttributeHex2
        ]);

        utility.addToScene(scene, [
            borderHex3.line,
            geoAttributeHex3
        ]);

        //download
        let hex1 = utility.createHexagon();
        hex1.position.set(0, -20*margin, 0);
        hex1.scale.set(scale, scale, 0);
        hex1.rotateZ(THREE.Math.degToRad(30));
        hex1.material.opacity = 0.6;
        scene.add(hex1);

        //upload
        let hex2 = utility.createHexagon();
        hex2.position.set(120*margin, 50, 0);
        hex2.scale.set(scale, scale, 0);
        hex2.rotateZ(THREE.Math.degToRad(30));
        hex2.material.opacity = 0.6;
        scene.add(hex2);

        //text
        let hex3 = utility.createHexagon();
        hex3.position.set(10*margin, 100*margin, 0);
        hex3.scale.set(60, 60, 0);
        hex3.rotateZ(THREE.Math.degToRad(30));
        hex3.material.opacity = 0.6;
        scene.add(hex3);


        const textHeadline = {
            MeshBasicMaterial: new THREE.MeshBasicMaterial({
                color: 0xffffff,
                transparent: true,
                opacity: 1,
                side: THREE.DoubleSide
            }),
            message: 'NetIO',
            fontSize: 23
        }

        const textUp = {
            MeshBasicMaterial: new THREE.MeshBasicMaterial({
                color: 0xffffff,
                transparent: true,
                opacity: 1,
                side: THREE.DoubleSide
            }),
            message: 'Upload',
            fontSize: 14
        }

        const textDown = {
            MeshBasicMaterial: new THREE.MeshBasicMaterial({
                color: 0xffffff,
                transparent: true,
                opacity: 1,
                side: THREE.DoubleSide
            }),
            message: 'Download',
            fontSize: 12
        }

        const loader = new FontLoader();
        let promiseFontLoader = new Promise((resolve, reject) => {
            loader.load('fonts/Roboto_Medium_Regular.json', async function (fontResponse) {
                resolve(fontResponse);
            })
        })

        new Promise((resolve, reject) => {
            Promise.resolve(promiseFontLoader).then(function (response) {
                const textHeadlineShape = response.generateShapes(textHeadline.message, textHeadline.fontSize);
                const textUpShape = response.generateShapes(textUp.message, textUp.fontSize);
                const textDownShape = response.generateShapes(textDown.message, textDown.fontSize);

                resolve({
                    objHeadline: {
                        textHeadline,
                        textHeadlineShape
                    },
                    objUp: {
                        textUp,
                        textUpShape
                    },
                    objDown: {
                        textDown,
                        textDownShape
                    }
                });
            })
        }).then((obj) => {

            const textHeadline = this.generateText(obj.objHeadline.textHeadlineShape, obj.objHeadline.textHeadline.MeshBasicMaterial);
            textHeadline.text.position.x = -30;
            textHeadline.text.position.y = 95;
            textHeadline.text.position.z = 10;
            scene.add(textHeadline.text);

            const textUp = this.generateText(obj.objUp.textUpShape, obj.objUp.textUp.MeshBasicMaterial)
            textUp.text.position.x = 100;
            textUp.text.position.y = 90;
            textUp.text.position.z = 10;
            scene.add(textUp.text);

            const textDown = this.generateText(obj.objDown.textDownShape, obj.objDown.textDown.MeshBasicMaterial)
            textDown.text.position.x = -35;
            textDown.text.position.y = 25;
            textDown.text.position.z = 10;
            scene.add(textDown.text);

        });

        return groupMatLineHex;
    }

    generateText(shapes, mbm)
    {
        const geometry = new THREE.ShapeGeometry(shapes);
        const bounding = geometry.computeBoundingBox();
        const text = new THREE.Mesh(geometry, mbm);
        const boundingBox = new THREE.Box3().setFromObject(text)
        return {
            geometry,
            bounding,
            text,
            boundingBox
        }
    }
}

export {NetworkHexagon}