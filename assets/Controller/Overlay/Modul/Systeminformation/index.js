import axios from "axios";
import {Camera} from "./Camera";
import * as THREE from "three";
import {Scene} from "./Scene";
import Utility from "../../Default/Utility";
//import {HexagonOverlay} from "./HexagonOverlay";
import {CpuHexagon} from "./Hexagon/Cpu/CpuHexagon";
import {NetworkHexagon} from "./Hexagon/Network/NetworkHexagon";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import * as TWEEN from "@tweenjs/tween.js";

class Systeminformation
{
    constructor() {
        this.loadWebsocket = 1;
        this.showInformation();
    }

    websocket() {
        axios({
            method: 'GET',
            params: {
                interval: 2000
            },
            url: '/overlay/module/systeminformation',
            responseType: 'json'
        });
    }

    async showInformation() {

        let renderer;

        const camera = await new Camera(THREE);
        const scene = await new Scene(THREE);

        let utility = new Utility(camera);

        //const hexagonOverlay = new HexagonOverlay(THREE, scene, camera, utility)
        //const groupMatLineHex = hexagonOverlay.displayHexagon();

        const cpuHexagon = new CpuHexagon(THREE, scene, camera, utility);
        const networkHexagon = new NetworkHexagon(THREE, scene, camera, utility);

        let groupMatLineNetworkHexagon = networkHexagon.setNetworkHexagon();
        let groupMatLineCpuHexagon = cpuHexagon.setCpuHexagon();


        if (this.loadWebsocket === 1) {
            this.websocket();

            let connection = new WebSocket('ws://localhost:6090/', 'echo-protocol');

            connection.onopen = function () {
                connection.send('Ping'); // Send the message 'Ping' to the server
            };

            // Log errors
            connection.onerror = function (error) {
                console.log('WebSocket Error ' + error);
            };

            connection.onmessage = function (e) {
                let data = JSON.parse(e.data);
                console.log(data);
                const incomingMb = data.incomingMb.toString();
                const outgoingMb = data.outgoingMb.toString();
            }

        }

        // RENDERER
        renderer = new THREE.WebGLRenderer( {
            alpha: true,
            antialias: true
        } );
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
        renderer.setPixelRatio( 1 );
        renderer.setSize( 1920, 1080 );
        renderer.setClearColor(0x000000, 0);

        const controls = new OrbitControls(camera, renderer.domElement);

        document.body.appendChild( renderer.domElement );


        const animate = function (time) {
            requestAnimationFrame( animate );
            TWEEN.update(time)

            //renderer.setViewport( -880, -450, 1920, 1080 );
            renderer.setViewport(0, 0, 1920, 1080 );
            renderer.domElement.id = 'systeminformation';
            renderer.domElement.style.zIndex = "20";
            render();
        };

        const render = function () {
            renderer.autoClear = false;


            let i;
            for (i = 0; i < groupMatLineNetworkHexagon.length; i++) {
                groupMatLineNetworkHexagon[i].resolution.set( 1920, 1080 );
            }

            let v;
            for (v = 0; v < groupMatLineCpuHexagon.length; v++) {
                groupMatLineCpuHexagon[v].resolution.set( 1920, 1080 );
            }
            renderer.clear();
            renderer.render(scene, camera);
        };

        const update = function () {

        }
        animate();
    }
}

export {Systeminformation}