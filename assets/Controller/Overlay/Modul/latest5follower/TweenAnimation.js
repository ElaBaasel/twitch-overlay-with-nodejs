import * as TWEEN from "@tweenjs/tween.js";

class TweenAnimation
{
    constructor(mesh, opacity, name) {
        this.opacity = opacity;
        this.name = name;
        this.setAnimation(mesh);
    }

    setAnimation(mesh) {

        let group = [];

        if (this.name === 'header') {


            let tween = new TWEEN.Tween(mesh.material)
                .to({
                    opacity: this.opacity
                }, 0)
                .onUpdate(update)

            group.push(tween);

            group[0].start();

        }
        if (this.name === 'body') {

            let tweenHead = new TWEEN.Tween(mesh.position)
                .to({
                    x: -178,
                    y: 30,
                    z: 0
                }, 2000)
                .easing(TWEEN.Easing.Elastic.In).onStart(() => {
                    mesh.userData.isTweening = true;
                })
                .onUpdate(update)

            let tweenBack = new TWEEN.Tween(mesh.position)
                .to({
                    x: -178,
                    y: -30,
                    z: 0
                }, 2000)
                .easing(TWEEN.Easing.Elastic.Out).onStart(() => {
                    mesh.userData.isTweening = true;
                })
                .onUpdate(update)

            tweenHead.chain(tweenBack);
            tweenBack.chain(tweenHead);
            tweenHead.start();
            //group.push(tweenHead);

            //group[0].chain(tweenBack);
            //tweenBack.chain(tweenHead)
            //groupTween[0].chain(groupTweenBack[0]);
            //group[0].start();

            //group[0].start();

        }

        const update = function () {

        }

    }
}

export {TweenAnimation}