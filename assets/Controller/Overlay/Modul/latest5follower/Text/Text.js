import {FontLoader} from "three/examples/jsm/loaders/FontLoader";
import * as THREE from "three";
import axios from "axios";

class Text
{

    constructor(THREE) {
        this.THREE = THREE;
        this.text = {};

        return new Promise((resolve, reject) => {
            Promise.resolve(this.setText()).then(function (response) {
                resolve(response);
            });
        });

    }

    setText() {

        const loader = new FontLoader();
        const THREE = this.THREE;

        return new Promise((resolve, reject) => {
            loader.load('fonts/Roboto_Medium_Regular.json', function (response) {

                const matLite = new THREE.MeshBasicMaterial({
                    color: 0xffffff,
                    transparent: true,
                    opacity: 1,
                    side: THREE.DoubleSide
                });

                const message = 'Latest 5 Follower';
                const shapes = response.generateShapes(message, 50);
                const geometry = new THREE.ShapeGeometry(shapes);

                geometry.computeBoundingBox();

                const text = new THREE.Mesh(geometry, matLite);
                let boundingBox = new THREE.Box3().setFromObject(text)

                text.position.x = -(boundingBox.max.x/2);
                text.position.y = -25;
                text.position.z = -50;

                resolve(text);
                //text
                //scene.add(text);

                /*            let numberOfFollower = 0;

                            setInterval(() => {
                                axios({
                                    method: 'POST',
                                    data: {
                                        follower: 'follower'
                                    },
                                    url: '/connector',
                                    responseType: 'json'
                                }).then(function (response) {

                                    let data = JSON.parse(response.data.data.data)
                                    let countData = data.total;
                                    if (countData !== numberOfFollower) {
                                        numberOfFollower = data.total;

                                        $.each(data.data, function (key, value) {
                                            if (key <= 4) {
                                                //console.log(value);
                                            }

                                        });

                                    }

                                })
                            }, 1000);*/

            })
        });

    }

}

export {Text}