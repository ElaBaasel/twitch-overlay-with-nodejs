import {FontLoader} from "three/examples/jsm/loaders/FontLoader";
import axios from "axios";
import * as TWEEN from "@tweenjs/tween.js";
import {Animation} from "../Animation";


class FolllowList
{

    constructor(THREE, scene, staticMixer) {
        this.THREE = THREE
        this.scene = scene
        this.mixer = staticMixer
        this.getMesh();

    }

    async getData() {

       return axios({
            method: 'POST',
            data: {
                follower: 'follower'
            },
            url: '/connector',
            responseType: 'json'
        }).then(function (response) {
            return response
        })

    }


    async getMesh() {
        const THREE = this.THREE;
        const scene = this.scene;
        const staticMixer = this.mixer;

        let animation = new Animation(THREE, scene)


        let numberOfFollower = 0;
        setInterval(() => {

            let newPromise = new Promise((resolve, reject) => {
                Promise.resolve(this.getData()).then(function (response) {

                    let data = JSON.parse(response.data.data.data)

                    let countData = data.total;
                    if (countData !== numberOfFollower) {
                        numberOfFollower = data.total;

                        let objectsToRemove = [];
                        scene.traverse(function(object) {
                            if (object.isMesh && object.name === 'followNames') {
                                objectsToRemove.push(object);
                            }
                        });

                        objectsToRemove.forEach(node => {
                            let parent = node.parent;
                            parent.remove( node );
                        });

                        let tweenGroup = [];
                        let tweenGroupBack = [];

                        let duration = 500;
                        let showTime = 3000;
                        let countFollows = 4; //0,1,2,3,4 => 5

                        data.data.forEach((value, key, array) => {
                            if (key <= countFollows) {

                                const loader = new FontLoader();
                                loader.load('fonts/Roboto_Medium_Regular.json', async function (fontResponse) {

                                    const matLite = new THREE.MeshBasicMaterial({
                                        color: 0xffffff,
                                        transparent: true,
                                        depthWrite: false,
                                        opacity: 0,
                                        side: THREE.DoubleSide,
                                        polygonOffset: true,
                                        polygonOffsetFactor: 1, // positive value pushes polygon further away
                                        polygonOffsetUnits: 1
                                    });

                                    const message = value.from_name
                                    //const message = 'ASCDEFBHIJKLMNOOOOOOOOOOO'

                                    const shapes = fontResponse.generateShapes(message, 50);

                                    const geometry = new THREE.ShapeGeometry(shapes);

                                    let bounding = geometry.computeBoundingBox();

                                    let text = new THREE.Mesh(geometry, matLite);

                                    let boundingBox = new THREE.Box3().setFromObject(text)

                                    text.position.x = -(boundingBox.max.x / 2);
                                    text.position.y = -225;
                                    text.position.z = -95;
                                    text.name = 'followNames'
                                    text.content = message;

                                    if (key === 0) {
                                        scene.add(text)

                                        let tweenStart = new TWEEN.Tween(text.material)
                                            .to({opacity: 1}, duration)
                                            .onUpdate(update)

                                        let tweenBack = new TWEEN.Tween(text.material)
                                            .to({opacity: 0}, duration)
                                            .delay(showTime)
                                            .onUpdate(update)
                                            .onComplete(() => setTimeout(() =>tweenGroup[key].start(),(showTime*countFollows)-(showTime*key)))

                                        tweenGroup.push(tweenStart);
                                        tweenGroupBack.push(tweenBack);

                                        tweenGroup[key].chain(tweenGroupBack[key]);

                                        tweenGroup[key].start();

/*                                        const animate = animation.setAnimation(text, text.position)
                                        animate.clipAction.syncWith(staticMixer[0])
                                        staticMixer.push(animate.mixer);*/

                                    }

                                    if (key === 1) {
                                        scene.add(text)

                                        let tweenStart = new TWEEN.Tween(text.material)
                                            .to({opacity: 1}, duration)
                                            .delay(showTime*key)
                                            .onUpdate(update)

                                        let tweenBack = new TWEEN.Tween(text.material)
                                            .to({opacity: 0}, duration)
                                            .delay(showTime)
                                            .onUpdate(update)
                                            .onComplete(() => setTimeout(() => tweenGroup[key].start(), (showTime*countFollows)-(showTime*key)))

                                        tweenGroup.push(tweenStart);
                                        tweenGroupBack.push(tweenBack);

                                        tweenGroup[key].chain(tweenGroupBack[key]);

                                        tweenGroup[key].start();

/*                                        const animate = animation.setAnimation(text, text.position)
                                        animate.clipAction.syncWith(staticMixer[0])
                                        staticMixer.push(animate.mixer);*/
                                    }

                                    if (key === 2) {
                                        scene.add(text)

                                        let tweenStart = new TWEEN.Tween(text.material)
                                            .to({opacity: 1}, duration)
                                            .delay(showTime*key)
                                            .onUpdate(update)

                                        let tweenBack = new TWEEN.Tween(text.material)
                                            .to({opacity: 0}, duration)
                                            .delay(showTime)
                                            .onUpdate(update)
                                            .onComplete(() => setTimeout(() => tweenGroup[key].start(), (showTime*countFollows)-(showTime*key)))

                                        tweenGroup.push(tweenStart);
                                        tweenGroupBack.push(tweenBack);

                                        tweenGroup[key].chain(tweenGroupBack[key]);

                                        tweenGroup[key].start();

/*                                        const animate = animation.setAnimation(text, text.position)
                                        animate.clipAction.syncWith(staticMixer[0])
                                        staticMixer.push(animate.mixer);*/
                                    }

                                    if (key === 3) {
                                        scene.add(text)

                                        let tweenStart = new TWEEN.Tween(text.material)
                                            .to({opacity: 1}, duration)
                                            .delay(showTime*key)
                                            .onUpdate(update)

                                        let tweenBack = new TWEEN.Tween(text.material)
                                            .to({opacity: 0}, duration)
                                            .delay(showTime)
                                            .onUpdate(update)
                                            .onComplete(() => setTimeout(() => tweenGroup[key].start(), (showTime*countFollows)-(showTime*key)))

                                        tweenGroup.push(tweenStart);
                                        tweenGroupBack.push(tweenBack);

                                        tweenGroup[key].chain(tweenGroupBack[key]);

                                        tweenGroup[key].start();

/*                                        const animate = animation.setAnimation(text, text.position)
                                        animate.clipAction.syncWith(staticMixer[0])
                                        staticMixer.push(animate.mixer);*/
                                    }

                                    if (key === 4) {
                                        scene.add(text)

                                        let tweenStart = new TWEEN.Tween(text.material)
                                            .to({opacity: 1}, duration)
                                            .delay(showTime*key)
                                            .onUpdate(update)

                                        let tweenBack = new TWEEN.Tween(text.material)
                                            .to({opacity: 0}, duration)
                                            .delay(showTime)
                                            .onUpdate(update)
                                            .onComplete(() => setTimeout(() => tweenGroup[key].start(), (showTime*countFollows)-(showTime*key)))

                                        tweenGroup.push(tweenStart);
                                        tweenGroupBack.push(tweenBack);

                                        tweenGroup[key].chain(tweenGroupBack[key]);

                                        tweenGroup[key].start();

/*                                        const animate = animation.setAnimation(text, text.position)
                                        animate.clipAction.syncWith(staticMixer[0])
                                        staticMixer.push(animate.mixer);*/
                                    }

                                    const update = function () {

                                    }

                                });

                            }

                        });

                    }

                });
            });

        }, 1000);


    }

}

export {FolllowList}