import RoundedRectShape from "./../../Shapes/roundedRectShape";


class ModalBody
{
    constructor(THREE) {
        this.THREE = THREE;


    }

    setBody() {
        const THREE = this.THREE;

        let roundedRectShape = new RoundedRectShape({
            x: -600,
            y: -60,
            width: 1200,
            height: 300,
            rounded: 20
        });
        let rectGeo = roundedRectShape.roundedRectShape();
        let shape = new THREE.ShapeGeometry(rectGeo);

        let mesh = new THREE.Mesh(shape, new THREE.MeshPhongMaterial({
            color: 0x000000,
            emissive: 0x29abe2,
            side: THREE.DoubleSide
        }));
        mesh.position.set( 0,-300, -155 );
        mesh.name = 'body';
        //mesh.castShadow = true; //default is false
        //mesh.receiveShadow = false; //default
        //mesh.rotation.x = Math.PI * 0.5;
        return mesh;
    }
}

export {ModalBody}