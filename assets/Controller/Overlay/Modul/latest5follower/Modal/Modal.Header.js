import RoundedRectShape from "./../../Shapes/roundedRectShape";

class ModalHeader
{
    constructor(THREE) {
        this.THREE = THREE;
    }

    header() {
        const THREE = this.THREE;

        let roundedRectShape = new RoundedRectShape({
            x: -600,
            y: -60,
            width: 1200,
            height: 120,
            rounded: 20
        });
        let rectGeo = roundedRectShape.roundedRectShape();
        let shape = new THREE.ShapeGeometry(rectGeo);

        let mesh = new THREE.Mesh(shape, new THREE.MeshPhongMaterial({
            color: 0x000000,
            emissive: 0x29abe2,
            side: THREE.DoubleSide
        }));
        mesh.position.set( 0, 0, -100 );
        mesh.name = 'header'
        mesh.castShadow = true; //default is false
        mesh.receiveShadow = false; //default
        //mesh.rotation.x = Math.PI * 0.5;
        return mesh;


    }

    setShadow() {
        const THREE = this.THREE;
        const planeGeometry = new THREE.PlaneGeometry(
            1300,
            160,
        );

        const material = new THREE.ShadowMaterial();
        material.opacity = 1;

        const plane = new THREE.Mesh(planeGeometry, material);
        plane.receiveShadow = true;

        plane.position.y = 0;
        plane.position.z = -150;
        return plane;
    }

    setBloom() {
        let roundedRectShape = new RoundedRectShape({
            x: -600,
            y: 0,
            width: 1200,
            height: 120,
            rounded: 20
        });

        let rectangleGeometry = roundedRectShape.roundedRectShape();

        let rectangleShape = new THREE.ShapeGeometry(rectangleGeometry);

        let meshPhong = new THREE.MeshPhongMaterial({
            emissive: 0xffffff,
            side: THREE.FrontSide,
            transparent: true
        })

        let rectangleMesh =  new THREE.Mesh(rectangleShape, meshPhong);

        rectangleMesh.layers.enable(1);

        return rectangleMesh;
    }

}

export {ModalHeader}