import * as THREE from "three";

import {Camera} from "./Camera";
import {Scene} from "./Scene";
import {Text} from "./Text/Text";
import {SpotLight} from "./Light/SpotLight";
import {ModalHeader} from './Modal/Modal.Header';
import {ModalBody} from './Modal/Modal.Body';
import {TweenAnimation} from "./TweenAnimation";
import * as TWEEN from "@tweenjs/tween.js";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {FolllowList} from "./Text/FolllowList";
import {Animation} from "./Animation";

class latest5follower
{

    constructor() {
        this.THREE = THREE
        this.build();
    }

    async build() {
        const THREE = this.THREE;

        let renderer;
        let clock;
        let staticMixer = [];

        const camera = await new Camera(THREE);
        const scene = await new Scene(THREE);

        //Mesh
        const text = await new Text(THREE);
        const followText = await new FolllowList(THREE, scene, staticMixer);
        const modalHeader = new ModalHeader(THREE);
        const modalBody = new ModalBody(THREE);
        const light = new SpotLight(THREE);
        let header = modalHeader.header();
        let shadow = modalHeader.setShadow();
        let body = modalBody.setBody();

        //scene
        scene.add(text);
        scene.add(header);
        scene.add(light);
        scene.add(shadow);
        scene.add(body);

        let animation = new Animation(THREE, scene)
        staticMixer.push(animation.setAnimation(body, body.position).mixer);
        staticMixer.push(animation.setAnimation(header, header.position).mixer);
        staticMixer.push(animation.setAnimation(text, text.position).mixer);

        //Tween
        //new TweenAnimation(header, 0.8, 'header');
        //new TweenAnimation(body, 0.8, 'body');

        // RENDERER
        renderer = new THREE.WebGLRenderer( {
            alpha: true,
            antialias: true
        } );
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
        renderer.setPixelRatio( 1 );
        renderer.setSize( 1920, 1080 );
        renderer.setClearColor(0x000000, 0);

        const controls = new OrbitControls( camera, renderer.domElement );
        controls.enabled = false;

        const duration = 2500;
        const position = new THREE.Vector3().copy(camera.position);
        const targetPosition = new THREE.Vector3( -222.4,  -147.8, 963.6);

        const positionBack = new THREE.Vector3().copy(targetPosition);
        const targetPositionBack = new THREE.Vector3( 222.4,  147.8, 963.6);

        let tweenStart = new TWEEN.Tween(position)
            .to(targetPosition, duration)
            .easing(TWEEN.Easing.Back.InOut)
            .onUpdate(function () {
                camera.position.copy(position);
                camera.lookAt(controls.target);
            })

        let tweenBack = new TWEEN.Tween(positionBack)
            .to(targetPositionBack, duration)
            .easing(TWEEN.Easing.Back.InOut)
            .onUpdate(function () {
                camera.position.copy(positionBack);
                camera.lookAt(controls.target);
            })

        let tweenMiddle = new TWEEN.Tween(positionBack)
            .to(targetPosition, duration)
            .easing(TWEEN.Easing.Back.InOut)
            .onUpdate(function () {
                camera.position.copy(positionBack);
                camera.lookAt(controls.target);
            })


        tweenStart.chain(tweenBack);
        tweenBack.chain(tweenMiddle);
        tweenMiddle.chain(tweenBack);
        tweenStart.start();

        document.body.appendChild( renderer.domElement );

        const animate = function (time) {
            requestAnimationFrame( animate );
            TWEEN.update(time)

            renderer.setViewport( 1150, -100, 700, 400 );
            renderer.domElement.id = 'last5follower';
            render();
        };

        clock = new THREE.Clock();

        const render = function () {
            renderer.autoClear = false;
            renderer.clear();

            const delta = clock.getDelta();

            if (staticMixer) {
                staticMixer.forEach(function (element, index) {
                    element.update(delta);
                });
            }

            renderer.render(scene, camera);
        };

        const update = function () {

        }
        animate();
    }



}



export {latest5follower}