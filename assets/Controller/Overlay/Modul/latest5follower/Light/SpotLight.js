class SpotLight
{
    constructor(THREE) {
        this.THREE = THREE;
        return this.setLight();
    }

    setLight() {
        const THREE = this.THREE;

        const light = new THREE.SpotLight( 0xffffff );
        light.castShadow = true; // default false
        light.position.z = 1200;
        light.position.y = 0;
        light.intensity = 1;
        light.angle = 1.240;
        light.power = 2;

        light.shadow.mapSize.width = 512; // default
        light.shadow.mapSize.height = 512; // default
        light.shadow.camera.near = 1; // default
        light.shadow.camera.far = 1500; // default
        light.shadow.focus = 1; // default

        return light;
    }
}

export {SpotLight}