class Animation
{
    /**
     *
     * @param THREE
     * @param scene
     */
    constructor(THREE, scene) {
        this.THREE = THREE
        this.scene = scene;
    }

    setAnimation(object, position) {

        const THREE = this.THREE
        const scene = this.scene;
        let mixer;

        const positionKF = new THREE.VectorKeyframeTrack('.position',
            [0, 1, 2], [
                position.x, position.y, position.z,
                position.x, position.y, position.z,
                position.x, position.y, position.z
            ]
        );

        // SCALE
        /*                const scaleKF = new THREE.VectorKeyframeTrack('.scale',
                            [0, 1, 2 ],
                            [1, 1, 1, 2, 2, 2, 1, 1, 1]
                        );*/

        // ROTATION
        // Rotation should be performed using quaternions, using a THREE.QuaternionKeyframeTrack
        // Interpolating Euler angles (.rotation property) can be problematic and is currently not supported

        // set up rotation about x axis
        const xAxis = new THREE.Vector3(0, 0, 0);


        /**
         *  Quaternion {_x: 0, _y: 0, _z: 0, _w: 1}
         *    _w: 1
         *    _x: 0
         *    _y: 0
         *    _z: 0
         *    w: 1
         *    x: 0
         *    y: 0
         *    z: 0
         */
        const qInitial = new THREE.Quaternion().setFromAxisAngle(xAxis, 0);

        /**
         *  Quaternion {_x: 0, _y: 0.25, _z: 0, _w: 6.123233995736766e-17}
         *    _w: 6.123233995736766e-17
         *    _x: 0
         *    _y: 0.25
         *    _z: 0
         *    w: 6.123233995736766e-17
         *    x: 0
         *    y: 0.25
         *    z: 0
         */
        const qFinal = new THREE.Quaternion().setFromAxisAngle(xAxis, Math.PI/2);

        /**
         * > createInterpolant: ƒ InterpolantFactoryMethodLinear( result )
         *   name: ".quaternion"
         * > times: Float32Array(3) [0, 1, 2, buffer: ArrayBuffer(12), byteLength: 12, byteOffset: 0, length: 3, Symbol(Symbol.toStringTag): 'Float32Array']
         * > values: Float32Array(12) [0, 0, 0, 1, 0, 0.25, 0, 6.123234262925839e-17, 0, 0, 0, 1, buffer: ArrayBuffer(48), byteLength: 48, byteOffset: 0, length: 12, Symbol(Symbol.toStringTag): 'Float32Array']
         */
        const quaternionKF = new THREE.QuaternionKeyframeTrack('.quaternion',
            [0, 1, 2],
            [qInitial.x, qInitial.y, qInitial.z, qInitial.w, qFinal.x, qFinal.y, qFinal.z, qFinal.w, qInitial.x, qInitial.y, qInitial.z, qInitial.w]
        );

        /**
         *   blendMode: 2500
         *   duration: 3
         *   name: "Action"
         * > tracks: (2) [VectorKeyframeTrack, QuaternionKeyframeTrack]
         */
        const clip = new THREE.AnimationClip('default', 3, [
            positionKF,
            quaternionKF
        ]);

        // setup the THREE.AnimationMixer
        mixer = new THREE.AnimationMixer(object);

        // create a ClipAction and set it to play
        const clipAction = mixer.clipAction(clip);
        clipAction.play();

        return {
            mixer: mixer,
            clipAction: clipAction
        }

    }

}
export {Animation}