class Camera
{

    constructor(THREE)
    {
        this.THREE = THREE;
        this.setWidth = 1920;
        this.setHeight = 1080;

        return new Promise((resolve, reject) => {
            Promise.resolve(this.setCamera()).then(function (response) {
                resolve(response);
            });
        });
    }

    setCamera()
    {
        const camera = new this.THREE.PerspectiveCamera( 80, this.setWidth / this.setHeight, 1, 1500 );
        camera.position.set( 0, 0, 1000 );

        //camera.layers.enable(1);
        return camera
    }

}

export {Camera}