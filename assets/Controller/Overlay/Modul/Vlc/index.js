import axios from "axios";

import * as THREE from "three";
import {Camera} from "./Camera";
import {Scene} from "./Scene";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import * as TWEEN from "@tweenjs/tween.js";
import {FontLoader} from "three/examples/jsm/loaders/FontLoader";

class Vlc
{

    constructor() {
        this.THREE = THREE
        this.drawTitle();
    }

    getVlc() {
        return axios({
            method: 'POST',
            data: {
                vlc: 'vlc'
            },
            url: '/overlay/module/vlc',
            responseType: 'json'
        }).then(function (response) {
            return response
        });
    }

    async drawTitle() {

        const THREE = this.THREE;

        let renderer;
        let clock;
        let staticMixer = [];

        const camera = await new Camera(THREE);
        const scene = await new Scene(THREE);

        let getCurrentText = '';
        setInterval(() => {
            let newPromise = new Promise((resolve, reject) => {
                Promise.resolve(this.getVlc()).then(function (response) {
                    let title = response.data.data

                    if (!title) {
                        let objectsToRemove = [];
                        scene.traverse(function(object) {
                            if (object.isMesh && object.name === 'vlcTitle') {
                                objectsToRemove.push(object);
                            }
                        });

                        objectsToRemove.forEach(node => {
                            let parent = node.parent;
                            parent.remove( node );
                        });
                    }

                    if (title) {
                    let regex = /(.*)\./g;
                    let regexTitle = regex.exec(title)[1];

                    if (getCurrentText !== regexTitle) {
                        getCurrentText = regexTitle;

                        let objectsToRemove = [];
                        scene.traverse(function(object) {
                            if (object.isMesh && object.name === 'vlcTitle') {
                                objectsToRemove.push(object);
                            }
                        });

                        objectsToRemove.forEach(node => {
                            let parent = node.parent;
                            parent.remove( node );
                        });

                        const loader = new FontLoader();
                        loader.load('fonts/Roboto_Light_Regular.json', async function (fontResponse) {

                            const matLite = new THREE.MeshBasicMaterial({
                                color: 0xffffff,
                                transparent: true,
                                depthWrite: false,
                                opacity: 1,
                                side: THREE.DoubleSide,
                                polygonOffset: true,
                                polygonOffsetFactor: 1, // positive value pushes polygon further away
                                polygonOffsetUnits: 1
                            });

                            const message = 'Current Track: '+regexTitle
                            //const message = 'ASCDEFBHIJKLMNOOOOOOOOOOO'

                            const shapes = fontResponse.generateShapes(message, 25);

                            const geometry = new THREE.ShapeGeometry(shapes);

                            let bounding = geometry.computeBoundingBox();

                            let text = new THREE.Mesh(geometry, matLite);

                            let boundingBox = new THREE.Box3().setFromObject(text)

                            text.position.x = -(boundingBox.max.x);
                            //text.position.y = -225;
                            //text.position.z = -95;
                            text.name = 'vlcTitle'
                            text.content = message;
                            scene.add(text);
                        });

                    }

                    }

                })
            })
        }, 5000);


/*        const size = 1000;
        const divisions = 10;

        const gridHelper = new THREE.GridHelper( size, divisions );
        scene.add( gridHelper );*/

        // RENDERER
        renderer = new THREE.WebGLRenderer( {
            alpha: true,
            antialias: true
        } );
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
        renderer.setPixelRatio( 1 );
        renderer.setSize( 1920, 1080 );
        renderer.setClearColor(0x000000, 0);

        const controls = new OrbitControls( camera, renderer.domElement );

        document.body.appendChild( renderer.domElement );

        const animate = function (time) {
            requestAnimationFrame( animate );
            TWEEN.update(time)

            //renderer.setViewport( 1150, -100, 700, 400 );
            renderer.setViewport( 840, 440, 1920, 1080 );
            renderer.domElement.id = 'vlc';
            render();
        };

        clock = new THREE.Clock();

        const render = function () {
            renderer.autoClear = false;
            renderer.clear();

/*            const delta = clock.getDelta();

            if (staticMixer) {
                staticMixer.forEach(function (element, index) {
                    element.update(delta);
                });
            }*/

            renderer.render(scene, camera);
        };

        const update = function () {

        }
        animate();

    }
}

export {Vlc}