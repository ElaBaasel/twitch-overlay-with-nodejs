import axios from "axios";
import {FontLoader} from "three/examples/jsm/loaders/FontLoader";
import RoundedRectShape from "../../Shapes/roundedRectShape";
import * as TWEEN from "@tweenjs/tween.js";

class Name
{

    constructor(THREE, scene) {
        this.THREE = THREE;
        this.scene = scene;
        this.setName();
    }


    async getUsersFollows() {

        return axios({
            method: 'POST',
            data: {
                follower: 'follower'
            },
            url: '/connector',
            responseType: 'json'
        }).then(function (response) {
            return response
        })

    }

    async getPlane(THREE, planeSettings) {
        let roundedRectShape = new RoundedRectShape({
            x: planeSettings.posX,
            y: -60,
            width: planeSettings.width,
            height: 150,
            rounded: 20
        });
        let rectGeo = roundedRectShape.roundedRectShape();
        let shape = new THREE.ShapeGeometry(rectGeo);

        let body = new THREE.Mesh(shape, new THREE.MeshPhongMaterial({
            color: 0x000000,
            emissive: 0x29abe2,
            side: THREE.DoubleSide
        }));
        body.position.set( 0,0, -155 );
        body.name = 'body';

        return body

    }

    setName() {
        const THREE = this.THREE;
        const scene = this.scene;

        const planeSettings = {
            width: 750,
            posX: -1000,
            planePadding: 40
        }

        let numberOfFollower = 0;

        new Promise((resolve, reject) => {
            Promise.resolve(this.getUsersFollows()).then(function (response) {
                let data = JSON.parse(response.data.data.data)
                numberOfFollower = data.total;
            })
        })

        setInterval(async() => {

            let promise = new Promise((resolve, reject) => {
                Promise.resolve(this.getUsersFollows()).then(function (response) {

                    let data = JSON.parse(response.data.data.data)

                    let countData = data.total;
                    if (countData > numberOfFollower) {
                        numberOfFollower = data.total;



                        const loader = new FontLoader();
                        loader.load('fonts/Roboto_Medium_Regular.json', async function (fontResponse) {

                            const matLite = new THREE.MeshBasicMaterial({
                                color: 0xffffff,
                                transparent: true,
                                depthWrite: false,
                                opacity: 1,
                                side: THREE.DoubleSide,
                                polygonOffset: true,
                                polygonOffsetFactor: 1, // positive value pushes polygon further away
                                polygonOffsetUnits: 1
                            });

                            const message = data.data[0].from_name
                            //const message = 'ASCDEFBHIJKLMNOOOOOOOOOOO'

                            const shapes = fontResponse.generateShapes(message, 25);

                            const geometry = new THREE.ShapeGeometry(shapes);

                            let bounding = geometry.computeBoundingBox();

                            let text = new THREE.Mesh(geometry, matLite);

                            let boundingBox = new THREE.Box3().setFromObject(text)
                            //console.log(-600+((boundingBox.max.x / 2)/2))
                            /*text.position.x = -(boundingBox.max.x / 2);
                            text.position.y = -225;
                            text.position.z = -95;*/
                            text.position.x = -(boundingBox.max.x / 2) - (planeSettings.posX*(-1)) + (planeSettings.width/2);
                            text.position.y = 0;
                            text.position.z = -150;
                            text.name = 'newFollow'
                            text.content = message;
                            //console.log(text);
                            scene.add(text);


                            resolve({
                                text, boundingBox
                            })





                        });

                    }


                })
            }).then((obj) => {
                //console.log(obj)
                new Promise((resolve, reject) => {
                    Promise.resolve(this.getPlane(THREE, planeSettings)).then(function (response) {
                        //console.log(response)

                        let fromXToX = 880;

                        const body = response;

                        scene.add(body);

                        let tweenStart = new TWEEN.Tween(body.position)
                            .to({
                                x: 0,
                                y: 0,
                                z: -155
                            }, 1000)
                            .onUpdate(update)

                        let tweenMiddle = new TWEEN.Tween(body.position)
                            .to({
                                x: fromXToX,
                                y: 0,
                                z: -155
                            }, 1000)
                            .onUpdate(update)

                        let tweenBack = new TWEEN.Tween(body.position)
                            .to({
                                x: 0,
                                y: 0,
                                z: -155
                            }, 1000)
                            .delay(3000)
                            .onUpdate(update)

                        tweenStart.chain(tweenMiddle);
                        tweenMiddle.chain(tweenBack);
                        tweenStart.start();

                        let positionX = -(obj.boundingBox.max.x / 2) - (planeSettings.posX*(-1)) + (planeSettings.width/2);
                        //console.log(positionX);
                        let textMeshTweenStart = new TWEEN.Tween(obj.text.position)
                            .to({
                                x: positionX,
                                y: 0,
                                z: -150
                            }, 1000)
                            .onUpdate(textUpdate)

                        let textMeshTweenMiddle = new TWEEN.Tween(obj.text.position)
                            .to({
                                x: fromXToX-(positionX*(-1)),
                                y: 0,
                                z: -150
                            }, 1000)
                            .onUpdate(textUpdate)

                        let textMeshTweenBack = new TWEEN.Tween(obj.text.position)
                            .to({
                                x: positionX,
                                y: 0,
                                z: -150
                            }, 1000)
                            .delay(3000)
                            .onUpdate(textUpdate)

                        textMeshTweenStart.chain(textMeshTweenMiddle);
                        textMeshTweenMiddle.chain(textMeshTweenBack);
                        textMeshTweenStart.start();


                        const update = function (time) {
                            console.log(time);

                        }
                        const textUpdate = function () {

                        }
                    })
                })

            });

/*            new Promise((res, rej) => {
                Promise.resolve(this.getPlane()).then(function (planeResponse) {
                    console.log(planeResponse);
                })
            })*/
        }, 1000);
    }

}

export {Name}