import * as THREE from "three";
import {Camera} from "./Camera";
import {Scene} from "./Scene";
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import * as TWEEN from "@tweenjs/tween.js";
import {Helper} from "../Helper/Helper";
import RoundedRectShape from "../Shapes/roundedRectShape";
import {Name} from "./Text/Name";

class newFollower
{
    constructor() {
        this.THREE = THREE
        this.build();
    }

    async build() {
        const THREE = this.THREE;

        let renderer;
        let clock;

        const camera = await new Camera(THREE);
        const scene = await new Scene(THREE);
        const name = new Name(THREE, scene);




        //const helper = new Helper(THREE)
        //scene.add(helper.GridHelper());

        renderer = new THREE.WebGLRenderer({
            alpha: true,
            antialias: true
        });
        renderer.shadowMap.enabled = true;
        renderer.shadowMap.type = THREE.PCFSoftShadowMap; // default THREE.PCFShadowMap
        renderer.setPixelRatio(1);
        renderer.setSize(1920, 1080);
        renderer.setClearColor(0x000000, 0);

        const controls = new OrbitControls(camera, renderer.domElement);

        document.body.appendChild(renderer.domElement);

        const animate = function (time) {
            requestAnimationFrame(animate);
            TWEEN.update(time)

            renderer.setViewport(0, 250, 700, 400);
            renderer.domElement.id = 'newFollower';
            render();
        };

        clock = new THREE.Clock();

        const render = function () {
            renderer.autoClear = false;
            renderer.clear();

            const delta = clock.getDelta();
            renderer.render(scene, camera);
        };

        const update = function () {

        }
        animate();
    }

}

export {newFollower}