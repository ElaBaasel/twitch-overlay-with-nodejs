class Scene
{

    constructor(THREE)
    {
        this.THREE = THREE;

        return new Promise((resolve, reject) => {
            Promise.resolve(this.setScene()).then(function (response) {
                resolve(response);
            });
        });

    }

    setScene()
    {
        const scene = new this.THREE.Scene();
        scene.background = null;
        //scene.background = new this.THREE.Color( 0xf0f0f0 );
        //scene.position.set(0,0,0);
        //scene.rotation.x = Math.PI * 0.5;
        //scene.rotation.x = Math.PI * 0.5;
        return scene;
    }

}

export {Scene}