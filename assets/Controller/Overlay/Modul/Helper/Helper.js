class Helper
{
    constructor(THREE) {
        this.THREE = THREE;
    }

    /**
     *
     * @return
     */
    GridHelper()
    {
        const THREE = this.THREE;

        const helper = new THREE.GridHelper( 2000, 100 );
        helper.position.y = 0;
        helper.material.opacity = 0.75;
        helper.material.transparent = false;
        return helper;
    }

    BloomPass()
    {
        const effectFXAA = new ShaderPass( FXAAShader )
        effectFXAA.uniforms.resolution.value.set( 1 / 1920, 1 / 1080 )
        effectFXAA.renderToScreen = true;
        effectFXAA.material.transparent = true;

        const bloomPass = new TransparentBackgroundFixedUnrealBloomPass(
            new THREE.Vector2(
                1920,
                1080),
            1.5,
            0.4,
            0.85
        );
        bloomPass.clearColor = 0x000000;
        bloomPass.nMips
        bloomPass.threshold = 0;
        bloomPass.strength = 1.5;
        bloomPass.radius = 0;

        const composer = new EffectComposer( renderer )
        const renderScene = new RenderPass( scene, camera )

        composer.addPass( renderScene )
        composer.addPass( effectFXAA )
        composer.addPass( bloomPass )
    }
}

export {Helper}