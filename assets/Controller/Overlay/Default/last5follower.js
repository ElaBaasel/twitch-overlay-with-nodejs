import * as THREE from 'three';
import {RenderPass} from "three/examples/jsm/postprocessing/RenderPass";
import {EffectComposer} from "three/examples/jsm/postprocessing/EffectComposer";
import {
    TransparentBackgroundFixedUnrealBloomPass,
    UnrealBloomPass
} from '../Modul/latest5follower/TransparentBackgroundFixedUnrealBloomPass';
import {FontLoader} from "three/examples/jsm/loaders/FontLoader";
import axios from "axios";
import RoundedRectShape from "../Modul/Shapes/roundedRectShape";
import * as TWEEN from "@tweenjs/tween.js";
import {UniformsUtils} from "three";
import {HorizontalBlurShader} from "three/examples/jsm/shaders/HorizontalBlurShader";
import {ShaderPass} from "three/examples/jsm/postprocessing/ShaderPass.js";
import {FXAAShader} from "three/examples/jsm/shaders/FXAAShader";


export function last5follower(){

    let groupTweenHex = [];
    let renderer;

    const camera = new THREE.PerspectiveCamera( 80, 1920 / 1080, 1, 1500 );
    camera.position.set( 0, 0, 1500 );
    camera.layers.enable(1);


    // SCENE
    const scene = new THREE.Scene();
    scene.background = null;


    const loader = new FontLoader();
    loader.load( 'fonts/Roboto_Medium_Regular.json', function (response) {

        const color = 0xffffff;

        const matLite = new THREE.MeshBasicMaterial( {
            color: color,
            transparent: true,
            opacity: 1,
            side: THREE.DoubleSide
        } );

        const message = 'Last 5 Follower';
        const shapes = response.generateShapes( message, 50 );
        const geometry = new THREE.ShapeGeometry( shapes );

        geometry.computeBoundingBox();

        const text = new THREE.Mesh( geometry, matLite );
        text.position.x = -250;
        text.position.y = 30;
        text.position.z = 50;
        scene.add( text );

        let numberOfFollower = 0;

        setInterval(() => {
            axios({
                method: 'POST',
                data: {
                    follower: 'follower'
                },
                url: '/connector',
                responseType: 'json'
            }).then(function (response) {

                let data = JSON.parse(response.data.data.data)
                let countData = data.total;
                if (countData !== numberOfFollower) {
                    numberOfFollower = data.total;

                    $.each( data.data, function( key, value ) {
                        if (key <= 4) {
                            //console.log(value);
                        }

                    });

                }

            })
        }, 1000);

    });

    let roundedRectShape = new RoundedRectShape({
        x: -600,
        y: 0,
        width: 1200,
        height: 120,
        rounded: 20
    });
    let shape = roundedRectShape.roundedRectShape();

    let geometrys = new THREE.ShapeGeometry(shape);

    let boxing =  new THREE.Mesh(geometrys, new THREE.MeshPhongMaterial({
        emissive: 0x29abe2,
        side: THREE.DoubleSide,
        transparent: true,
    }));

    boxing.position.set( 0, 0, 20 );
    scene.add( boxing );

    let head = new TWEEN.Tween(boxing.material)
        .to({
            opacity: 0.85
        }, 0)
        .onUpdate(update)

    //groupTweenHex.push(box);
    groupTweenHex.push(head);

    groupTweenHex[0].start();

    let roundedRectShapeShadow = new RoundedRectShape({
        x: -612.5,
        y: -10,
        width: 1225,
        height: 140,
        rounded: 20
    });

    let shapeShadow = roundedRectShapeShadow.roundedRectShape();

    let geometrysShadow = new THREE.ShapeGeometry(shapeShadow);

    let boxingShadow =  new THREE.Mesh(geometrysShadow, new THREE.MeshPhongMaterial({
        emissive: 0x000000,
        side: THREE.DoubleSide,
        transparent: true,
        depthTest: false
    }));

    boxingShadow.position.set( 0, 0, 10 );
    scene.add( boxingShadow );

    let headShadow = new TWEEN.Tween(boxingShadow.material)
        .to({
            opacity: 0.1
        }, 0)
        .onUpdate(update)

    groupTweenHex.push(headShadow);
    groupTweenHex[1].start();



    let roundedRectShapeBody = new RoundedRectShape({
        x: -600,
        y: -290,
        width: 1200,
        height: 300,
        rounded: 20
    });
    let shapeBody = roundedRectShapeBody.roundedRectShape();

    let geometryBody = new THREE.ShapeGeometry(shapeBody);

    const material = new THREE.ShaderMaterial({
        uniforms: UniformsUtils.clone(HorizontalBlurShader.uniforms),
        vertexShader: HorizontalBlurShader.vertexShader,
        fragmentShader: HorizontalBlurShader.fragmentShader,
    });

    let meshPhong = new THREE.MeshPhongMaterial({
        emissive: 0x29abe2,
        side: THREE.DoubleSide,
        transparent: true
    })

    //scene.add( meshPhong );


    let boxingBody =  new THREE.Mesh(geometryBody, meshPhong);
    boxingBody.layers.enable(1);

    boxingBody.position.set( 0, 0, 5 );
    scene.add( boxingBody );

    let body = new TWEEN.Tween(boxingBody.material)
        .to({
            opacity: 0.8
        }, 0)
        .onUpdate(update)

    groupTweenHex.push(body);
    groupTweenHex[2].start();



    // RENDERER
    renderer = new THREE.WebGLRenderer( {
        alpha: true,
        antialias: true
    } );

    renderer.setPixelRatio( 1 );
    renderer.setSize( 1920, 1080 );
    renderer.setClearColor(0xff0000, 0);
    document.body.appendChild( renderer.domElement );

    const effectFXAA = new ShaderPass( FXAAShader )
    effectFXAA.uniforms.resolution.value.set( 1 / 1920, 1 / 1080 )
    effectFXAA.renderToScreen = true;
    effectFXAA.material.transparent = true;

    const bloomPass = new TransparentBackgroundFixedUnrealBloomPass(
        new THREE.Vector2(
            1920,
            1080),
        4,
        1,
        0.1
    );

    const composer = new EffectComposer( renderer )
    const renderScene = new RenderPass( scene, camera )

    composer.addPass( renderScene )
    composer.addPass( effectFXAA )
    composer.addPass( bloomPass )


    const animate = function (time) {
        requestAnimationFrame( animate );
        TWEEN.update(time)

        renderer.setViewport( 0, 0, 1920, 1080 );
        renderer.domElement.id = 'last5follower';
        render();
    };

    const render = function () {
        renderer.autoClear = false;
        renderer.clear();

        camera.layers.set(1);
        composer.render();

        renderer.clearDepth();
        camera.layers.set(0);

        renderer.render( scene, camera );
    };

    const update = function () {

    }
    animate();


}
