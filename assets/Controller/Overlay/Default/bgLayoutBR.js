import * as THREE from 'three';
import { TWEEN } from 'three/examples/jsm/libs/tween.module.min';
import Utility from './Utility.js';


export function bgLayoutBR(){
    let renderer, scene, camera;

    let groupMatLineHex = [];
    let groupTween = [];
    let groupTweenBack = [];

    let groupTweenHex = [];
    let groupTweenBackHex = [];

    let clock = new THREE.Clock();
    let delta = 0;

    init();
    animate();

    function init() {

        scene = new THREE.Scene();

        camera = new THREE.PerspectiveCamera( 80, 1920 / 1080, 1, 1000 );
        camera.position.set(-295, 137, 200 );

        let utility = new Utility(camera);

        let margin = 1.05;

        // 93278f << lila
        // 29abe2 << blau

        let borderHex1 = utility.addBorderLine(0x29abe2);
        groupMatLineHex.push(borderHex1.matLine);
        let geoAttributeHex1 = utility.setGeoAttribute(borderHex1.matLine, borderHex1);

        let borderHex2 = utility.addBorderLine(0x29abe2);
        groupMatLineHex.push(borderHex2.matLine);
        let geoAttributeHex2 = utility.setGeoAttribute(borderHex2.matLine, borderHex2);


        let borderHex3 = utility.addBorderLine(0x93278f);
        groupMatLineHex.push(borderHex3.matLine);
        let geoAttributeHex3 = utility.setGeoAttribute(borderHex3.matLine, borderHex3);

        let borderHex4 = utility.addBorderLine(0x29abe2);
        groupMatLineHex.push(borderHex4.matLine);
        let geoAttributeHex4 = utility.setGeoAttribute(borderHex4.matLine, borderHex4);

        let borderHex5 = utility.addBorderLine(0x93278f);
        groupMatLineHex.push(borderHex5.matLine);
        let geoAttributeHex5 = utility.setGeoAttribute(borderHex5.matLine, borderHex5);

        let borderHex6 = utility.addBorderLine(0x93278f);
        groupMatLineHex.push(borderHex6.matLine);
        let geoAttributeHex6 = utility.setGeoAttribute(borderHex6.matLine, borderHex5);

        let borderHex7 = utility.addBorderLine(0x29abe2);
        groupMatLineHex.push(borderHex7.matLine);
        let geoAttributeHex7 = utility.setGeoAttribute(borderHex7.matLine, borderHex5);

        const lineWidth = 1000/camera.position.z*utility.lineMultiplier;
        const scaleSet = (camera.position.z/1000)*lineWidth+10;

        borderHex1.line.scale.set(scaleSet, scaleSet, 0);
        borderHex1.line.position.set(0, 10*margin, 0);
        borderHex1.line.rotateZ(THREE.Math.degToRad(30));
        borderHex1.matLine.opacity = 0.2;

        borderHex2.line.scale.set(scaleSet, scaleSet, 0);
        borderHex2.line.position.set(-20*margin, 0, 0);
        borderHex2.line.rotateZ(THREE.Math.degToRad(30));
        borderHex2.matLine.opacity = 0.2;


        borderHex3.line.scale.set(scaleSet, scaleSet, 0);
        borderHex3.line.position.set(-40*margin, -10*margin, 0);
        borderHex3.line.rotateZ(THREE.Math.degToRad(30));
        borderHex3.matLine.opacity = 0.2;

        borderHex4.line.scale.set(scaleSet, scaleSet, 0);
        borderHex4.line.position.set(0, -30*margin, 0);
        borderHex4.line.rotateZ(THREE.Math.degToRad(30));
        borderHex4.matLine.opacity = 0.2;

        borderHex5.line.scale.set(scaleSet, scaleSet, 0);
        borderHex5.line.position.set(0, -10*margin, 0);
        borderHex5.line.rotateZ(THREE.Math.degToRad(30));
        borderHex5.matLine.opacity = 0.2;

        borderHex6.line.scale.set(scaleSet, scaleSet, 0);
        borderHex6.line.position.set(-20*margin, -20*margin, 0);
        borderHex6.line.rotateZ(THREE.Math.degToRad(30));
        borderHex6.matLine.opacity = 0.2;

        borderHex7.line.scale.set(scaleSet, scaleSet, 0);
        borderHex7.line.position.set(-40*margin, -30*margin, 0);
        borderHex7.line.rotateZ(THREE.Math.degToRad(30));
        borderHex7.matLine.opacity = 0.2;


        let tween1 = new TWEEN.Tween(borderHex1.matLine)
            .to({opacity: 0.8}, 1000)
            .onUpdate(update)

        let tweenBack1 = new TWEEN.Tween(borderHex1.matLine)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTween[0].start(), 7000))


        let tween2 = new TWEEN.Tween(borderHex2.matLine)
            .to({opacity: 0.8}, 1000)
            .delay(1000)
            .onUpdate(update)

        let tweenBack2 = new TWEEN.Tween(borderHex2.matLine)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTween[1].start(), 6000))



        let tween3 = new TWEEN.Tween(borderHex3.matLine)
            .to({opacity: 0.8}, 1000)
            .delay(2000)
            .onUpdate(update)

        let tweenBack3 = new TWEEN.Tween(borderHex3.matLine)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTween[2].start(), 5000))


        let tween4 = new TWEEN.Tween(borderHex4.matLine)
            .to({opacity: 0.8}, 1000)
            .delay(3000)
            .onUpdate(update)

        let tweenBack4 = new TWEEN.Tween(borderHex4.matLine)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTween[3].start(), 4000))


        let tween5 = new TWEEN.Tween(borderHex5.matLine)
            .to({opacity: 0.8}, 1000)
            .delay(4000)
            .onUpdate(update)

        let tweenBack5 = new TWEEN.Tween(borderHex5.matLine)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTween[4].start(), 3000))

        let tween6 = new TWEEN.Tween(borderHex6.matLine)
            .to({opacity: 0.8}, 1000)
            .delay(5000)
            .onUpdate(update)

        let tweenBack6 = new TWEEN.Tween(borderHex6.matLine)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTween[5].start(), 2000))


        let tween7 = new TWEEN.Tween(borderHex7.matLine)
            .to({opacity: 0.8}, 1000)
            .delay(6000)
            .onUpdate(update)

        let tweenBack7 = new TWEEN.Tween(borderHex7.matLine)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTween[6].start(), 1000))


        groupTween.push(tween1);
        groupTweenBack.push(tweenBack1);
        groupTween.push(tween2);
        groupTweenBack.push(tweenBack2);
        groupTween.push(tween3);
        groupTweenBack.push(tweenBack3);
        groupTween.push(tween4);
        groupTweenBack.push(tweenBack4);
        groupTween.push(tween5);
        groupTweenBack.push(tweenBack5);
        groupTween.push(tween6);
        groupTweenBack.push(tweenBack6);
        groupTween.push(tween7);
        groupTweenBack.push(tweenBack7);

        groupTween[0].chain(groupTweenBack[0]);
        groupTween[1].chain(groupTweenBack[1]);
        groupTween[2].chain(groupTweenBack[2]);
        groupTween[3].chain(groupTweenBack[3]);
        groupTween[4].chain(groupTweenBack[4]);
        groupTween[5].chain(groupTweenBack[5]);
        groupTween[6].chain(groupTweenBack[6]);

        groupTween[0].start();
        groupTween[1].start();
        groupTween[2].start();
        groupTween[3].start();
        groupTween[4].start();
        groupTween[5].start();
        groupTween[6].start();



        utility.addToScene(scene, [
            borderHex1.line,
            geoAttributeHex1
        ]);
        utility.addToScene(scene, [
            borderHex2.line,
            geoAttributeHex2
        ]);

        utility.addToScene(scene, [
            borderHex3.line,
            geoAttributeHex3
        ]);
        utility.addToScene(scene, [
            borderHex4.line,
            geoAttributeHex4
        ]);
        utility.addToScene(scene, [
            borderHex5.line,
            geoAttributeHex5
        ]);
        utility.addToScene(scene, [
            borderHex6.line,
            geoAttributeHex6
        ]);
        utility.addToScene(scene, [
            borderHex7.line,
            geoAttributeHex7
        ]);


        let hex1 = utility.createHexagon();
        hex1.position.set(0, 10*margin, 0);
        hex1.scale.set(10, 10, 0);
        hex1.rotateZ(THREE.Math.degToRad(30));
        scene.add(hex1);

        let hex2 = utility.createHexagon();
        hex2.position.set(-20*margin, 0, 0);
        hex2.scale.set(10, 10, 0);
        hex2.rotateZ(THREE.Math.degToRad(30));
        scene.add(hex2);

        let hex3 = utility.createHexagon();
        hex3.position.set(-40*margin, -10*margin, 0);
        hex3.scale.set(10, 10, 0);
        hex3.rotateZ(THREE.Math.degToRad(30));
        scene.add(hex3);

        let hex4 = utility.createHexagon();
        hex4.position.set(0, -30*margin, 0);
        hex4.scale.set(10, 10, 0);
        hex4.rotateZ(THREE.Math.degToRad(30));
        scene.add(hex4);

        let hex5 = utility.createHexagon();
        hex5.position.set(0, -10*margin, 0);
        hex5.scale.set(10, 10, 0);
        hex5.rotateZ(THREE.Math.degToRad(30));
        scene.add(hex5);

        let hex6 = utility.createHexagon();
        hex6.position.set(-20*margin, -20*margin, 0);
        hex6.scale.set(10, 10, 0);
        hex6.rotateZ(THREE.Math.degToRad(30));
        scene.add(hex6);

        let hex7 = utility.createHexagon();
        hex7.position.set(-40*margin, -30*margin, 0);
        hex7.scale.set(10, 10, 0);
        hex7.rotateZ(THREE.Math.degToRad(30));
        scene.add(hex7);

        let tween1Hex = new TWEEN.Tween(hex1.material)
            .to({opacity: 0.8}, 1000)
            .onUpdate(update)

        let tweenBack1Hex = new TWEEN.Tween(hex1.material)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTweenHex[0].start(), 7000))

        let tween2Hex = new TWEEN.Tween(hex2.material)
            .to({opacity: 0.8}, 1000)
            .delay(1000)
            .onUpdate(update)

        let tweenBack2Hex = new TWEEN.Tween(hex2.material)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTweenHex[1].start(), 6000))



        let tween3Hex = new TWEEN.Tween(hex3.material)
            .to({opacity: 0.8}, 1000)
            .delay(2000)
            .onUpdate(update)

        let tweenBack3Hex = new TWEEN.Tween(hex3.material)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTweenHex[2].start(), 5000))


        let tween4Hex = new TWEEN.Tween(hex4.material)
            .to({opacity: 0.8}, 1000)
            .delay(3000)
            .onUpdate(update)

        let tweenBack4Hex = new TWEEN.Tween(hex4.material)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTweenHex[3].start(), 4000))


        let tween5Hex = new TWEEN.Tween(hex5.material)
            .to({opacity: 0.8}, 1000)
            .delay(4000)
            .onUpdate(update)

        let tweenBack5Hex = new TWEEN.Tween(hex5.material)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTweenHex[4].start(), 3000))

        let tween6Hex = new TWEEN.Tween(hex6.material)
            .to({opacity: 0.8}, 1000)
            .delay(5000)
            .onUpdate(update)

        let tweenBack6Hex = new TWEEN.Tween(hex6.material)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTweenHex[5].start(), 2000))

        let tween7Hex = new TWEEN.Tween(hex7.material)
            .to({opacity: 0.8}, 1000)
            .delay(6000)
            .onUpdate(update)

        let tweenBack7Hex = new TWEEN.Tween(hex7.material)
            .to({opacity: 0.2}, 1000)
            .onUpdate(update)
            .onComplete(() => setTimeout(() => groupTweenHex[6].start(), 1000))


        groupTweenHex.push(tween1Hex);
        groupTweenBackHex.push(tweenBack1Hex);
        groupTweenHex.push(tween2Hex);
        groupTweenBackHex.push(tweenBack2Hex);
        groupTweenHex.push(tween3Hex);
        groupTweenBackHex.push(tweenBack3Hex);
        groupTweenHex.push(tween4Hex);
        groupTweenBackHex.push(tweenBack4Hex);
        groupTweenHex.push(tween5Hex);
        groupTweenBackHex.push(tweenBack5Hex);
        groupTweenHex.push(tween6Hex);
        groupTweenBackHex.push(tweenBack6Hex);
        groupTweenHex.push(tween7Hex);
        groupTweenBackHex.push(tweenBack7Hex);

        groupTweenHex[0].chain(groupTweenBackHex[0]);
        groupTweenHex[1].chain(groupTweenBackHex[1]);
        groupTweenHex[2].chain(groupTweenBackHex[2]);
        groupTweenHex[3].chain(groupTweenBackHex[3]);
        groupTweenHex[4].chain(groupTweenBackHex[4]);
        groupTweenHex[5].chain(groupTweenBackHex[5]);
        groupTweenHex[6].chain(groupTweenBackHex[6]);

        groupTweenHex[0].start();
        groupTweenHex[1].start();
        groupTweenHex[2].start();
        groupTweenHex[3].start();
        groupTweenHex[4].start();
        groupTweenHex[5].start();
        groupTweenHex[6].start();



        let manager = new THREE.LoadingManager();
        manager.onLoad = function ( ) {
            setOpacity (hex5, 0.5);
        };

        renderer = new THREE.WebGLRenderer({
            alpha: true,
            antialias: true
        });
        renderer.setPixelRatio(1);
        renderer.setSize(1920, 1080);
        document.body.appendChild( renderer.domElement );



    }

    function update() {

    }

    function onWindowResize() {

        camera.aspect = 1920 / 1080;
        camera.updateProjectionMatrix();
        renderer.setSize( 1920, 1080 );

    }


    function animate(time) {

        requestAnimationFrame( animate );
        TWEEN.update(time)

        // main scene

        //renderer.setClearColor( 0x000000, 0 );

        renderer.setViewport( 0, 0, 1920, 1080 );
        renderer.domElement.id = 'bottomRight';

        delta = clock.getDelta();

        let i;
        for (i = 0; i < groupMatLineHex.length; i++) {
            groupMatLineHex[i].resolution.set( 1920, 1080 );
        }

        renderer.clear();
        renderer.render( scene, camera );

    }
}