import * as THREE from 'three';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry';
import { TWEEN } from 'three/examples/jsm/libs/tween.module.min';

export function animateName(){
    THREE.Cache.enabled = true;

    let camera, cameraTarget, scene, renderer;
    let group, textMesh1, textMesh2, textGeo, materials;
    let text = 'ElaBaasel',

        bevelEnabled = true,
        font = undefined; // normal bold

    const height = 20,
        size = 70,
        hover = 30,

        curveSegments = 4,

        bevelThickness = 2,
        bevelSize = 1.5;

    const fontMap = {
        'helvetiker': 0,
        'optimer': 1,
        'gentilis': 2,
        'droid/droid_sans': 3,
        'droid/droid_serif': 4
    };

    const weightMap = {
        'regular': 0,
        'bold': 1
    };

    const reverseFontMap = [];
    const reverseWeightMap = [];
    let groupTween = [];

    for ( const i in fontMap ) reverseFontMap[ fontMap[ i ] ] = i;
    for ( const i in weightMap ) reverseWeightMap[ weightMap[ i ] ] = i;

    let targetRotation = 0;

    init();
    animate();

    function init() {

       // CAMERA
        camera = new THREE.PerspectiveCamera( 80, 1920 / 1080, 100, 1000 );
        camera.position.set( 0, -150, 800 );

        cameraTarget = new THREE.Vector3( 0, 0, 0 );

        // SCENE
        scene = new THREE.Scene();
        scene.background = null;
        scene.position.x = -300;

        // LIGHTS
        const pointLight = new THREE.PointLight( 0xffffff , 1.0);
        pointLight.position.set( 0, 180, 300 );
        scene.add( pointLight );

        //29abe2
        const pointLightBlue = new THREE.PointLight( 0x29abe2 , 1.0);
        pointLightBlue.position.set( 0, -360, 45 );
        scene.add( pointLightBlue );

/*        const sphereSize = 50;
        const pointLightHelper1 = new THREE.PointLightHelper( pointLight, sphereSize );
        scene.add( pointLightHelper1 );


        const pointLightHelper = new THREE.PointLightHelper( pointLightBlue, sphereSize );
        scene.add( pointLightHelper );*/

        materials = [
            new THREE.MeshPhongMaterial( { color: 0xffffff, flatShading: true } ), // front
            new THREE.MeshPhongMaterial( { color: 0xffffff } ) // side
        ];

        group = new THREE.Group();
        group.position.y = 0;

        scene.add( group );

        const loader = new FontLoader();
        loader.load( 'fonts/satisfy-v11-latin-regular.json', function (response) {

            font = response;

            textGeo = new TextGeometry(text, {

                font: font,

                size: size,
                height: height,
                curveSegments: curveSegments,

                bevelThickness: bevelThickness,
                bevelSize: bevelSize,
                bevelEnabled: bevelEnabled

            });

            textGeo.computeBoundingBox();

            const centerOffset = - 0.5 * ( textGeo.boundingBox.max.x - textGeo.boundingBox.min.x );

            let textMesh;
            textMesh = new THREE.Mesh( textGeo, materials );

            textMesh.position.x = centerOffset;
            textMesh.position.y = hover;
            textMesh.position.z = 0;

            textMesh.rotation.x = 0;
            textMesh.rotation.y = Math.PI * 2;

            let tweenStart = new TWEEN.Tween(textMesh.position)
                .to({
                    x: -178,
                    y: 30,
                    z: 0
                }, 1500).easing(TWEEN.Easing.Elastic.Out).onStart(() => {
                    textMesh.userData.isTweening = true;
                });

            let tweenBack = new TWEEN.Tween(textMesh.position)
                .to({
                    x: -178,
                    y: 60,
                    z: 0
                }, 1500).easing(TWEEN.Easing.Elastic.In).onStart(() => {
                    textMesh.userData.isTweening = true;
                });


            groupTween.push(tweenStart);

            groupTween[0].chain(tweenBack);
            tweenBack.chain(tweenStart)
            //groupTween[0].chain(groupTweenBack[0]);
            groupTween[0].start();


            group.add( textMesh );

        });


/*        const plane = new THREE.Mesh(
            new THREE.PlaneGeometry( 100, 100 ),
            new THREE.MeshBasicMaterial( { color: 0x000000, opacity: 0, transparent: true } )
        );

        scene.add( plane );*/


        // RENDERER
        renderer = new THREE.WebGLRenderer( {
            alpha: true,
            antialias: true
        } );

        renderer.setPixelRatio( 1 );
        renderer.setSize( 1920, 1080 );
        renderer.setClearColor( 0xffffff, 0 );
        renderer.autoClear = false;
        document.body.appendChild( renderer.domElement );

    }


    function animate(time) {

        requestAnimationFrame( animate );
        TWEEN.update(time)

        renderer.setViewport( -132, 835, 700, 400 );
        renderer.domElement.id = 'animateName';
        renderer.domElement.style.zIndex = "10";
        render();

    }

    function render() {

        group.rotation.y += ( targetRotation - group.rotation.y ) * 0.05;

        camera.lookAt( cameraTarget );

        renderer.clear();
        renderer.render( scene, camera );

    }

}