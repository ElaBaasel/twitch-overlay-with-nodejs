import * as THREE from 'three';
//import { TWEEN } from 'three/examples/jsm/libs/tween.module.min';
import TWEEN from '@tweenjs/tween.js';
import Utility from './Utility.js';


export function bgLayoutTL(){
        let renderer, scene, camera;


        let groupMatLineHex = [];
        let groupUserNameBorder = [];
        let groupTween = [];
        let groupTweenBack = [];

        let groupTweenHex = [];
        let groupTweenBackHex = [];

        let clock = new THREE.Clock();
        let delta = 0;

    init();
        animate();

        function init() {



            scene = new THREE.Scene();

            camera = new THREE.PerspectiveCamera( 80, 1920 / 1080, 100, 1000 );
            camera.position.set(247, -159, 200);

            let utility = new Utility(camera);

            let margin = 1.1;

            // 93278f << lila
            // 29abe2 << blau

            let userNameBorder = utility.userNameBorder(0x29abe2);
            groupMatLineHex.push(userNameBorder.matLine);
            let geoAttributeUserName = utility.setGeoAttribute(userNameBorder.matLine, userNameBorder);

            let borderHex3 = utility.addBorderLine(0x93278f);
            groupMatLineHex.push(borderHex3.matLine);
            let geoAttributeHex3 = utility.setGeoAttribute(borderHex3.matLine, borderHex3);

            let borderHex4 = utility.addBorderLine(0x29abe2);
            groupMatLineHex.push(borderHex4.matLine);
            let geoAttributeHex4 = utility.setGeoAttribute(borderHex4.matLine, borderHex4);

            let borderHex5 = utility.addBorderLine(0x93278f);
            groupMatLineHex.push(borderHex5.matLine);
            let geoAttributeHex5 = utility.setGeoAttribute(borderHex5.matLine, borderHex5);

            let borderHex6 = utility.addBorderLine(0x93278f);
            groupMatLineHex.push(borderHex6.matLine);
            let geoAttributeHex6 = utility.setGeoAttribute(borderHex6.matLine, borderHex6);

            const lineWidth = 1000/camera.position.z*utility.lineMultiplier;
            const scaleSet = (camera.position.z/1000)*lineWidth+10;

            userNameBorder.line.scale.set(scaleSet, scaleSet, 0);
            userNameBorder.line.position.set(-20, 0, 0);
            userNameBorder.matLine.opacity = 0.8;

            borderHex3.line.scale.set(scaleSet, scaleSet, 0);
            borderHex3.line.position.set(-40*margin, 0, 0);
            borderHex3.matLine.opacity = 0.8;

            borderHex4.line.scale.set(scaleSet, scaleSet, 0);
            borderHex4.line.position.set(-50*margin, -17*margin, 0);
            borderHex4.matLine.opacity = 0.8;

            borderHex5.line.scale.set(scaleSet, scaleSet, 0);
            borderHex5.line.position.set(-30*margin, -17*margin, 0);
            borderHex5.matLine.opacity = 0.8;

            borderHex6.line.scale.set(scaleSet, scaleSet, 0);
            borderHex6.line.position.set(-40*margin, -34*margin, 0);
            borderHex6.matLine.opacity = 0.8;

            utility.addToScene(scene, [
                userNameBorder.line,
                geoAttributeUserName
            ]);
            utility.addToScene(scene, [
                borderHex3.line,
                geoAttributeHex3
            ]);
            utility.addToScene(scene, [
                borderHex4.line,
                geoAttributeHex4
            ]);
            utility.addToScene(scene, [
                borderHex5.line,
                geoAttributeHex5
            ]);

            utility.addToScene(scene, [
                borderHex6.line,
                geoAttributeHex6
            ]);



            let hexUsername = utility.createHexagonUsername();
            hexUsername.position.set(-20, 0, 0);
            hexUsername.scale.set(10, 10, 0);
            scene.add(hexUsername);

            let hex3 = utility.createHexagon();
            hex3.position.set(-40*margin, 0, 0);
            hex3.scale.set(10, 10, 0);
            scene.add(hex3);

            let hex4 = utility.createHexagon();
            hex4.position.set(-50*margin, -17*margin, 0);
            hex4.scale.set(10, 10, 0);
            scene.add(hex4);

            let hex5 = utility.createHexagon();
            hex5.position.set(-30*margin, -17*margin, 0);
            hex5.scale.set(10, 10, 0);
            scene.add(hex5);

            let hex6 = utility.createHexagon();
            hex6.position.set(-40*margin, -34*margin, 0);
            hex6.scale.set(10, 10, 0);
            scene.add(hex6);


            let tween2HexUsername = new TWEEN.Tween(hexUsername.material)
                .to({opacity: 0.8}, 1000)
                .onUpdate(update)

            let tweenBack2HexUsername = new TWEEN.Tween(hexUsername.material)
                .to({opacity: 0.2}, 1000)
                .onUpdate(update)
                .onComplete(() => setTimeout(() => groupTweenHex[0].start(), 4000))

            let tween3Hex = new TWEEN.Tween(hex3.material)
                .to({opacity: 0.8}, 1000)
                .delay(1000)
                .onUpdate(update)

            let tweenBack3Hex = new TWEEN.Tween(hex3.material)
                .to({opacity: 0.2}, 1000)
                .onUpdate(update)
                .onComplete(() => setTimeout(() => groupTweenHex[1].start(), 3000))


            let tween4Hex = new TWEEN.Tween(hex4.material)
                .to({opacity: 0.8}, 1000)
                .delay(2000)
                .onUpdate(update)

            let tweenBack4Hex = new TWEEN.Tween(hex4.material)
                .to({opacity: 0.2}, 1000)
                .onUpdate(update)
                .onComplete(() => setTimeout(() => groupTweenHex[2].start(), 2000))


            let tween5Hex = new TWEEN.Tween(hex5.material)
                .to({opacity: 0.8}, 1000)
                .delay(3000)
                .onUpdate(update)

            let tweenBack5Hex = new TWEEN.Tween(hex5.material)
                .to({opacity: 0.2}, 1000)
                .onUpdate(update)
                .onComplete(() => setTimeout(() => groupTweenHex[3].start(), 1000))

            let tween6Hex = new TWEEN.Tween(hex6.material)
                .to({opacity: 0.8}, 1000)
                .delay(4000)
                .onUpdate(update)

            let tweenBack6Hex = new TWEEN.Tween(hex6.material)
                .to({opacity: 0.2}, 1000)
                .onUpdate(update)
                .onComplete(() => setTimeout(() => groupTweenHex[4].start(), 0))


            groupTweenHex.push(tween2HexUsername);
            groupTweenBackHex.push(tweenBack2HexUsername);

            groupTweenHex.push(tween3Hex);
            groupTweenBackHex.push(tweenBack3Hex);
            groupTweenHex.push(tween4Hex);
            groupTweenBackHex.push(tweenBack4Hex);
            groupTweenHex.push(tween5Hex);
            groupTweenBackHex.push(tweenBack5Hex);
            groupTweenHex.push(tween6Hex);
            groupTweenBackHex.push(tweenBack6Hex);

            groupTweenHex[0].chain(groupTweenBackHex[0]);
            groupTweenHex[1].chain(groupTweenBackHex[1]);
            groupTweenHex[2].chain(groupTweenBackHex[2]);
            groupTweenHex[3].chain(groupTweenBackHex[3]);
            groupTweenHex[4].chain(groupTweenBackHex[4]);

            groupTweenHex[0].start();
            groupTweenHex[1].start();
            groupTweenHex[2].start();
            groupTweenHex[3].start();
            groupTweenHex[4].start();






/*            let manager = new THREE.LoadingManager();
            manager.onLoad = function ( ) {
                setOpacity (hex5, 0.5);
            };*/

            renderer = new THREE.WebGLRenderer({
                alpha: true,
                antialias: true
            });

            renderer.setPixelRatio( 1 );
            renderer.setSize( 1920, 1080 );
            document.body.appendChild( renderer.domElement );



        }

        function update() {

        }

        function onWindowResize() {

            camera.aspect = 1920 / 1080;
            camera.updateProjectionMatrix();
            renderer.setSize( 1920, 1080 );

        }


        function animate(time) {

            requestAnimationFrame( animate );
            TWEEN.update(time)

            // main scene

            //renderer.setClearColor( 0x000000, 0 );

            renderer.setViewport( 0, 0, 1920, 1080 );
            renderer.domElement.id = 'topLeft';

            delta = clock.getDelta();

            let i;
            for (i = 0; i < groupMatLineHex.length; i++) {
                groupMatLineHex[i].resolution.set( 1920, 1080 );
            }


            renderer.clear();
            renderer.render( scene, camera );

        }
    }
