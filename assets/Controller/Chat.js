import {Controller} from "stimulus"
import axios from "axios";
import $ from "jquery";
import tmi from "tmi.js";

export default class extends Controller {

    async connect() {
        let url = this.element.dataset.urlChat;

        $('#chatConnect').click(function (e) {
            let buttonNameSave = this.name;
            axios({
                data: {
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
            });
        });


        $('#chatCloseConnection').click(function (e) {
            let buttonNameSave = this.name;
            axios({
                data: {
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                console.log(res);
            });
        });


        const client = new tmi.Client({
            options: { debug: false },
            identity: {
                username: 'elabaasel',
                password: 'oauth:'+chatToken
            },
            channels: [ 'elabaasel' ]
        });
        client.connect().catch(console.error);

        let chatMsg = 0;
        let chatMsgHeight = 0

        client.on('message', (channel, tags, message, self) => {
            // Ignore echoed messages.
            if(self) return;

            let chatMsgRow = '<div class="row chatMsgRow" />';
            let chatContent = '<div class="nickname"><span>'+tags["display-name"]+'</span></div><div class="col-8 msg">'+message+'</div>';
            let container = $(chatMsgRow).append(chatContent);

            chatMsg += 1;
            $('.chatMsg').append(container);
            let $chatContainer = $('.chat');
            let $getLatestMsg = $('.chatMsg > .chatMsgRow:last-child');
            let $findNickname = $getLatestMsg.find('.nickname');
            let getMsgWidth = $findNickname.children().width();

            $findNickname.width(getMsgWidth);

            let chatContainerHeight = $chatContainer.height();
            chatMsgHeight += $getLatestMsg.height();

            let getChatMsgHeight = $getLatestMsg.height();

            if (chatMsgHeight >= chatContainerHeight) {

                let $allMsg = $('.chatMsg > .chatMsgRow');

                $.each($allMsg, function(key,value) {
                    let height = $(value).height();
                    let $allChatMsgHeight = $('.chatMsg');
                    if ($allChatMsgHeight.height() >= chatContainerHeight) {
                        $allMsg[key].remove();
                    }
                    if ($allChatMsgHeight.height() <= chatContainerHeight) {
                        return false;
                    }
                });

            }
        });

    }
}