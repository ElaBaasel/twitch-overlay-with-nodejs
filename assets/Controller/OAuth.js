import {Controller} from "stimulus"
import axios from "axios";
import $ from "jquery";

export default class extends Controller {

    connect(){

        let url = this.element.dataset.urlOauth;
        let CSRFToken = document.getElementById("oAuthToken").value;


        $('#genUrl').click(function (e) {

            let buttonNameSave = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                let url = '<a href="'+res.url+'">'+res.url+'<a>';
                $('.generatedUrl').html(url);
            });
        });

        $('#oAuthSubmitButton').click(function (e) {

            let oAuthClientId = document.getElementById("oAuthClientId").value;
            let oAuthClientSecret = document.getElementById("oAuthClientSecret").value;
            let buttonNameSave = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    oAuthClientId: oAuthClientId,
                    oAuthClientSecret: oAuthClientSecret,
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res) {
                    document.getElementById("oAuthClientId").value = '';
                    document.getElementById("oAuthClientSecret").value = '';
                }
                if (res.msg === 'error') {
                    $('.empty-input').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()

                }
            });

        });

        $('#oAuthShowClientId').click(function (e) {

            const buttonName = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    oAuthShowClientId: buttonName
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                if (res.msg === 'info') {
                    $('.showClientId').remove();
                    $('.toastBox').prepend(res.html);
                    const toastMsg = document.getElementById("toastMsg");
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }
            });
        });

        $('#oAuthShowClientSecret').click(function (e) {

            const buttonName = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    oAuthShowClientSecret: buttonName
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                if (res.msg === 'info') {
                    $('.showClientSecret').remove();
                    $('.toastBox').prepend(res.html);
                    const toastMsg = document.getElementById("toastMsg");
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }
            });
        });

        $('.selectScopes > div').click(function (e) {
            $(this).toggleClass('clickedScope');
        });

        $('#oAuthGenerateAccessToken').click(function (e) {

            const buttonName = this.name;

            const scopes = $('.selectScopes > .clickedScope');
            let text = []
            $.each(scopes, function(key, value) {
                text.push($(value).text())
            });
            let scopeList = text.join(" ");

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    oAuthGenerateAccessToken: buttonName,
                    scopeList: scopeList
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                if (res.msg === 'info') {
                    $('.genOAuthToken').remove();
                    $('.toastBox').prepend(res.html);
                    const toastMsg = document.getElementById("toastMsg");
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }

            });

        });

        $('#oAuthShowAccessToken').click(function (e) {

            const buttonName = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    oAuthShowAccessToken: buttonName
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;
                if (res.msg === 'info') {
                    $('.showOAuthToken').remove();
                    $('.toastBox').prepend(res.html);
                    const toastMsg = document.getElementById("toastMsg");
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }

            });

        });

        $('#oAuthTokenSubmitButton').click(function (e) {

            let oAuthToken = document.getElementById("oAuthAccessToken").value;
            let buttonNameSave = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    oAuthToken: oAuthToken,
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'error') {
                    $('.alert-warning-token').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()

                }
                if (res.msg === 'info') {
                    document.getElementById("oAuthAccessToken").value = '';

                    $('.token-input').remove();
                    $('.toastBox').prepend(res.html);
                    const toastMsg = document.getElementById("toastMsg");
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }
            });

        });

        $('#oAuthReturnHashSubmitButton').click(function (e) {

            let oAuthHashToken = document.getElementById("returnHash").value;
            let buttonNameSave = this.name;

            axios({
                withCredentials: true,
                headers: {
                    'CSRF-Token': CSRFToken
                },
                data: {
                    oAuthHashToken: oAuthHashToken,
                    buttonNameSave: buttonNameSave
                },
                method: 'POST',
                url: url,
                dataType: "json"

            }).then(function (response) {
                let res = response.data;

                if (res.msg === 'error') {
                    $('.alert-warning-token').remove();
                    $('.toastBox').prepend(res.html);

                    const toastMsg = document.getElementById('toastMsg');
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()

                }
                if (res.msg === 'info') {
                    document.getElementById("oAuthAccessToken").value = '';

                    $('.token-input').remove();
                    $('.toastBox').prepend(res.html);
                    const toastMsg = document.getElementById("toastMsg");
                    const toast = new bootstrap.Toast(toastMsg);
                    toast.show()
                }
            });

        });
        //EventListener on window.location.hash
        if (typeof window.location.hash && window.location.hash !== '') {
            const regexp = new RegExp("([^&].*?)=([^&]+)", "g");
            const hashArray = regexp.exec(window.location.hash);
            console.log(hashArray[1])
            if (hashArray[1] === '#access_token') {
                const getToken = hashArray[2];
                console.log(getToken);
                $('#returnHash').val(getToken);
            }
        }

    }

}